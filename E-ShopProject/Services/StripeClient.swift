import Foundation
import Stripe
import Alamofire

class StripeClient {

    static let shared = StripeClient()
    var baseURLString: String?
    var baseURL: URL {
        if let urlString = self.baseURLString,
            let url = URL(string: urlString) {
            return url
        } else {
            fatalError()
        }
    }

    private init() {}

    func createAndConformPayment(_ token: STPToken, amount: Int, completion: @escaping (_ error: Error?) -> Void) {

        let serializer = DataResponseSerializer(emptyResponseCodes: Set([200, 204, 205]))
        let url = self.baseURL.appendingPathComponent("charge")

        let params: [String: Any] = [
            "stripeToken": token.tokenId,
            "amount": amount,
            "description": Constants.defaultDescription,
            "currency": Constants.defaultCurrency
        ]

        AF.request(url, method: .post, parameters: params).validate().response(responseSerializer: serializer) { (response) in
            switch response.result {
            case .success(let success):
                print("Payment succefull, \(success)")
                completion(nil)
            case .failure(let error):
                print("Проблема: ", error.localizedDescription)
                completion(error)
            }
        }
    }
}
