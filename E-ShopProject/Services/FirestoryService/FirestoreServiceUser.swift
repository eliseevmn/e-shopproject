import Foundation
import FirebaseFirestore

class FirestoreServiceUser {

    // MARK: - DownloadUser
    static func dowloadUserFromFirestore(userId: String, email: String) {
        firebaseReference(.user).document(userId).getDocument { (snapshot, _) in
            guard let snapshot = snapshot else {
                return
            }

            if snapshot.exists {
                self.saveUserLocally(mUserDictionary: snapshot.data()! as NSDictionary)
            } else {
                let user = MUser(objetcId: userId, email: email, firstName: "", lastName: "")
                self.saveUserLocally(mUserDictionary: self.userDictionaryFrom(user: user))
                self.saveUserToFirestore(mUser: user)
            }
        }
    }

    // MARK: - Save User to Firebase
    static func saveUserToFirestore(mUser: MUser) {
        firebaseReference(.user).document(mUser.objetcId).setData(userDictionaryFrom(user: mUser) as? [String: Any] ?? [:]) { (error) in
            if error != nil {
                print("Error saving user \(String(describing: error?.localizedDescription))")
            }
        }
    }

    static func saveUserLocally(mUserDictionary: NSDictionary) {
        UserDefaults.standard.set(mUserDictionary, forKey: kCURRENTUSER)
        UserDefaults.standard.synchronize()
    }

    // MARK: - Helper function
    static func userDictionaryFrom(user: MUser) -> NSDictionary {
        return NSDictionary(objects: [user.objetcId, user.email,
                                      user.firstName, user.lastName,
                                      user.fullName, user.fullAddress ?? "",
                                      user.onBoard, user.purchasedItemIds],
                            forKeys: [kOBJECTID as NSCopying,
                                      kEMAIL as NSCopying,
                                      kFIRSTNAME as NSCopying,
                                      kLASTNAME as NSCopying,
                                      kFULLNAME as NSCopying,
                                      kFULLADRESS as NSCopying,
                                      kONBOARD as NSCopying,
                                      kPURCHASEDITEMSIDS as NSCopying])
    }

    // MARK: - Update user
    static func updateCurrentUserInFirestore(withValues: [String: Any], completion: @escaping (_ error: Error?) -> Void) {
        if let dictionary = UserDefaults.standard.object(forKey: kCURRENTUSER) {
            let userObject = (dictionary as? NSDictionary)?.mutableCopy() as? NSMutableDictionary
            userObject?.setValuesForKeys(withValues)

            firebaseReference(.user).document(MUser.currentId()).updateData(withValues) { (error) in
                completion(error)
                if error == nil {
                    self.saveUserLocally(mUserDictionary: userObject ?? NSMutableDictionary())
                }
            }
        }
    }
}
