import Foundation
import FirebaseAuth

class FirestoreServiceLogin {

    // MARK: - Login func
    static func loginUserWith(email: String, password: String, completion: @escaping(_ error: Error?, _ isEmailVerified: Bool) -> Void) {
        Auth.auth().signIn(withEmail: email, password: password) { (authDataResult, error) in
            guard let authDataResult = authDataResult else {
                completion(error, false)
                return
            }

            if error == nil {
                if authDataResult.user.isEmailVerified {
                    FirestoreServiceUser.dowloadUserFromFirestore(userId: authDataResult.user.uid, email: email)
                    completion(error, true)
                } else {
                    print("Email is not verified")
                    completion(error, false)
                }
            } else {
                completion(error, false)
            }
        }
    }

    // MARK: - Register User
    static func registerUserWith(email: String, password: String, completino: @escaping (_ error: Error?) -> Void) {
        Auth.auth().createUser(withEmail: email, password: password) { (authDataResult, error) in
            guard let authDataResult = authDataResult else {
                return
            }

            if error == nil {
                authDataResult.user.sendEmailVerification { (error) in
                    print(error?.localizedDescription ?? "Auth email verification error")
                }
            }
        }
    }

    // MARK: - Resend link methods
    static func resetPasswordFor(email: String, completion: @escaping(_ error: Error?) -> Void) {
        Auth.auth().sendPasswordReset(withEmail: email) { (error) in
            completion(error)
        }
    }

    static func resendVerificationEmail(email: String, completion: @escaping(_ error: Error?) -> Void) {
        Auth.auth().currentUser?.reload(completion: { (error) in
            Auth.auth().currentUser?.sendEmailVerification(completion: { (error) in
                print(error?.localizedDescription ?? "Resend email error")
                completion(error)
            })
        })
    }

    static func logOutCurrentUser(completion: @escaping(_ error: Error?) -> Void) {
        do {
            try Auth.auth().signOut()
            UserDefaults.standard.removeObject(forKey: kCURRENTUSER)
            UserDefaults.standard.synchronize()
            completion(nil)
        } catch let error as NSError {
            completion(error)
        }
    }
}
