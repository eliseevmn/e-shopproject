import Foundation
import FirebaseFirestore

class FirestoryServiceItems {

    // MARK: - Save items
    static func saveItemsToFirestore(item: Item) {
        firebaseReference(.items).document(item.idItem).setData(itemDictionaryfrom(item: item) as? [String: Any] ?? [:])
    }

    // MARK: - Helper functions
    static func itemDictionaryfrom(item: Item) -> NSDictionary {
        return NSDictionary(objects: [item.idItem,
                                      item.categoryId,
                                      item.name, item.description,
                                      item.price, item.imageLinks],
                            forKeys: [kOBJECTID as NSCopying,
                                      kCATEGORYID as NSCopying,
                                      kNAME as NSCopying,
                                      kDESCRIPTION as NSCopying,
                                      kPRICE as NSCopying,
                                      kIMAGELINKS as NSCopying])
    }

    // MARK: - Download func
    static func downloadItemsFromFirestore(withCategoryId: String, completion: @escaping (_ itemArray: [Item]) -> Void) {
        var itemArray: [Item] = []

        firebaseReference(.items).whereField(kCATEGORYID, isEqualTo: withCategoryId).getDocuments { (snapshot, error) in
            if let error = error {
                print(error.localizedDescription)
                completion([])
                return
            }
            guard let snapshot = snapshot else {
                completion(itemArray)
                return
            }
            if !snapshot.isEmpty {
                for itemDictionary in snapshot.documents {
                    itemArray.append(Item(dictionary: itemDictionary.data() as NSDictionary))
                }
            }
            completion(itemArray)
        }
    }

    static func downloadItems(withIds: [String], completion: @escaping (_ itemArray: [Item]) -> Void) {
        var count = 0
        var itemArray: [Item] = []

        if withIds.count > 0 {
            for itemId in withIds {
                firebaseReference(.items).document(itemId).getDocument { (snapshot, _) in
                    guard let snapshot = snapshot else {
                        completion(itemArray)
                        return
                    }

                    if snapshot.exists {
                        itemArray.append(Item(dictionary: snapshot.data()! as NSDictionary))
                        count += 1
                    } else {
                        completion(itemArray)
                    }

                    if count == withIds.count {
                        completion(itemArray)
                    }
                }
            }
        } else {
            completion(itemArray)
        }
    }

    // MARK: - Algolia func
    static func saveItemsToAlgolia(item: Item) {
        let index = AlgoliaService.shared.index

        let itemToSave = itemDictionaryfrom(item: item) as? [String: Any] ?? [:]
        index.addObject(itemToSave, withID: item.idItem, requestOptions: nil) { (_, error) in
            if error != nil {
                print("error saving to Algolia, ", error!.localizedDescription)
            } else {
                print("Added to algolia")
            }
        }
    }
}
