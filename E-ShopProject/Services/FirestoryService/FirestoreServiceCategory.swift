import Foundation
import FirebaseFirestore

class FirestoryServiceCategory {

    // MARK: - Download category from firebase
    static func downloadCategoriesFromFirebase(completion: @escaping (_ categoryArray: [Category]) -> Void) {
        var categoryArray: [Category] = []

        firebaseReference(.category).getDocuments { (snapshot, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }

            guard let snapshot = snapshot else {
                completion(categoryArray)
                return
            }

            if !snapshot.isEmpty {
                for categoryDictionary in snapshot.documents {
                    categoryArray.append(Category(dictionary: categoryDictionary.data() as NSDictionary))
                }
            }
            completion(categoryArray)
        }
    }

    // MARK: - Save category function
    static func saveCategoryToFirebase(category: Category) {
        let idCategory = UUID().uuidString
        category.idCategory = idCategory

        firebaseReference(.category).document(idCategory).setData(categoryDictionaryFrom(category) as? [String: Any] ?? [:])
    }

    // MARK: - Helpers
    static func categoryDictionaryFrom(_ category: Category) -> NSDictionary {
        return NSDictionary(objects: [category.idCategory ?? "",
                                      category.name ?? "",
                                      category.imageName ?? ""],
                            forKeys: [kOBJECTID as NSCopying,
                                      kNAME as NSCopying,
                                      kIMAGENAME as NSCopying])
    }

    // MARK: - Createdatabase: use only one time for test
    static func createCategorySet() {
        let womenClothing = Category(name: "Women's Clothing & Accessories", imageName: "womenCloth")
        let footWaer = Category(name: "Footwaer", imageName: "footWaer")
        let electronics = Category(name: "Electronics", imageName: "electronics")
        let menClothing = Category(name: "Men's Clothing & Accessories", imageName: "menCloth")
        let health = Category(name: "Health & Beauty", imageName: "health")
        let baby = Category(name: "Baby Stuff", imageName: "baby")
        let home = Category(name: "Home & Kitchen", imageName: "home")
        let car = Category(name: "Automobiles & Motorcyles", imageName: "car")
        let luggage = Category(name: "Luggage & bags", imageName: "luggage")
        let jewelery = Category(name: "Jewelery", imageName: "jewelery")
        let hobby =  Category(name: "Hobby, Sport, Traveling", imageName: "hobby")
        let pet = Category(name: "Pet products", imageName: "pet")
        let industry = Category(name: "Industry & Business", imageName: "industry")
        let garden = Category(name: "Garden supplies", imageName: "garden")
        let camera = Category(name: "Cameras & Optics", imageName: "camera")

        let arrayOfCategories = [womenClothing,
                                 footWaer,
                                 electronics,
                                 menClothing,
                                 health,
                                 baby,
                                 home,
                                 car,
                                 luggage,
                                 jewelery,
                                 hobby,
                                 pet,
                                 industry,
                                 garden,
                                 camera]

        for category in arrayOfCategories {
            saveCategoryToFirebase(category: category)
        }
    }
}
