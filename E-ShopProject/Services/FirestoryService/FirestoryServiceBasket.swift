import Foundation
import FirebaseFirestore

class FirestoreServiceBasket {

    // MARK: - Download items
    static func downloadBasketFromFirestore(ownerId: String, completion: @escaping(_ basket: Basket?) -> Void) {
        firebaseReference(.basket).whereField(kOWNERID, isEqualTo: ownerId).getDocuments { (snapshot, _) in
            guard let snapshot = snapshot else {
                completion(nil)
                return
            }

            if !snapshot.isEmpty && snapshot.documents.count > 0 {
                let basket = Basket(dictionary: snapshot.documents.first!.data() as NSDictionary)
                completion(basket)
            } else {
                completion(nil)
            }
        }
    }

    // MARK: - Save to Firebase
    static func saveBasketToFirestore(basket: Basket) {
        firebaseReference(.basket).document(basket.idBasket).setData(basketDictionaryfrom(basket: basket) as? [String: Any] ?? [:])
    }

    // MARK: - Helper functions
    static func basketDictionaryfrom(basket: Basket) -> NSDictionary {
        return NSDictionary(objects: [basket.idBasket,
                                      basket.ownerId,
                                      basket.itemIds],
                            forKeys: [kOBJECTID as NSCopying,
                                      kOWNERID as NSCopying,
                                      kITEMSIDS as NSCopying])
    }

    // MARK: - Update basket
    static func updateBasketInFirestore(basket: Basket, withValues: [String: Any], completion: @escaping(_ error: Error?) -> Void) {
        firebaseReference(.basket).document(basket.idBasket).updateData(withValues) { (error) in
            completion(error)
        }
    }
}
