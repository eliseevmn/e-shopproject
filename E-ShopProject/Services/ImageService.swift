import Foundation
import FirebaseFirestore
import FirebaseStorage

class ImageService {

    static let storage = Storage.storage()

    // загрузка изображений в Firebase
    static func uploadImages(images: [UIImage], itemId: String, completion: @escaping (_ imageLinks: [String]) -> Void) {

        if Reachabilty.hasConnection() {

            var uploadedImagesCount = 0
            var imageLinkArray: [String] = []
            var nameSuffix = 0

            for image in images {

                let fileName = "ItemImages/" + itemId + "/" + "\(nameSuffix)" + ".jpg"
                let imageData = image.jpegData(compressionQuality: 0.5)

                saveImageInFirebase(imageData: imageData!, filename: fileName) { (imageLink) in
                    if imageLink != nil {
                        imageLinkArray.append(imageLink!)
                        uploadedImagesCount += 1
                        if uploadedImagesCount == images.count {
                            completion(imageLinkArray)
                        }
                    }
                }
                nameSuffix += 1
            }
        } else {
            print("No Internet Connection")
        }
    }

    static func saveImageInFirebase(imageData: Data, filename: String, completion: @escaping(_ imageLink: String?) -> Void) {
        var task: StorageUploadTask!
        let storageRef = storage.reference(forURL: kFILEPREFERENCE).child(filename)
        task = storageRef.putData(imageData, metadata: nil, completion: { (_, error) in
            task.removeAllObservers()
            if error != nil {
                print("Error uploading image", error?.localizedDescription ?? "")
                completion(nil)
                return
            }

            storageRef.downloadURL { (url, _) in
                guard let downloadUrl = url else {
                    completion(nil)
                    return
                }
                completion(downloadUrl.absoluteString)
            }
        })
    }

    static func downloadImages(imageUrls: [String], completion: @escaping(_ images: [UIImage]) -> Void) {
        var imageArray: [UIImage] = []
        var downloadCounter = 0

        for link in imageUrls {
            let url = NSURL(string: link)
            let downloadQueue = DispatchQueue(label: "imageDownloadQueue")
            downloadQueue.async {
                downloadCounter += 1
                let data = NSData(contentsOf: url! as URL)
                if data != nil {
                    imageArray.append(UIImage(data: data! as Data)!)

                    if downloadCounter == imageArray.count {
                        DispatchQueue.main.async {
                            completion(imageArray)
                        }
                    }
                } else {
                    print("Couldn't download image")
                    completion(imageArray)
                }
            }
        }
    }

}
