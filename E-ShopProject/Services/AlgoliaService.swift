import Foundation
import InstantSearchClient

class AlgoliaService {

    static let shared = AlgoliaService()
    let client = Client(appID: kAlgoliaAppId, apiKey: kAlgoliaAdminKey)
    let index = Client(appID: kAlgoliaAppId, apiKey: kAlgoliaAdminKey).index(withName: "item_Name")

    private init() {}

    func searchAlgolia(searchString: String, completion: @escaping(_ itemArray: [String]) -> Void) {

        let index = AlgoliaService.shared.index
        var resultsIds: [String] = []

        let query = Query(query: searchString)

        query.attributesToRetrieve = ["name", "description"]

        index.search(query) { (content, error) in

            if error == nil {
                let cont = content!["hits"] as? [[String: Any]] ?? [[:]]

                resultsIds = []

                for result in cont {
                    resultsIds.append(result["objectID"] as? String ?? "")
                }

                completion(resultsIds)
            } else {
                print("Error algolia search ", error!.localizedDescription)
                completion(resultsIds)
            }
        }
    }
}
