import Foundation
import FirebaseFirestore

class Item: Hashable {
    var idItem: String
    var categoryId: String
    var name: String
    var description: String
    var price: Double
    var imageLinks: [String]

    init(idItem: String, categoryId: String, name: String, description: String, price: Double, imageLinks: [String]) {
        self.idItem = idItem
        self.categoryId = categoryId
        self.name = name
        self.description = description
        self.price = price
        self.imageLinks = imageLinks
    }

    init(dictionary: NSDictionary) {
        idItem = dictionary[kOBJECTID] as? String ?? ""
        categoryId = dictionary[kCATEGORYID] as? String ?? ""
        name = dictionary[kNAME] as? String ?? ""
        description = dictionary[kDESCRIPTION] as? String ?? ""
        price = dictionary[kPRICE] as? Double ?? 0.0
        imageLinks = dictionary[kIMAGELINKS] as? [String] ?? []
    }

    static func == (lhs: Item, rhs: Item) -> Bool {
        return lhs.idItem == rhs.idItem
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(idItem)
    }

    func contains(filter: String?) -> Bool {
        guard let filter = filter else { return true }
        if filter.isEmpty { return true }
        let lowercasedFilter = filter.lowercased()
        return name.lowercased().contains(lowercasedFilter)
    }
}
