import UIKit
import FirebaseFirestore
import FirebaseStorage

class Category: Hashable {
    var idCategory: String?
    var name: String?
    var image: UIImage?
    var imageName: String?

    init(name: String, imageName: String) {
        self.idCategory = ""
        self.name = name
        self.imageName = imageName
        image = UIImage(named: imageName)
    }

    init(dictionary: NSDictionary) {
        idCategory = dictionary[kOBJECTID] as? String
        name = dictionary[kNAME] as? String
        image = UIImage(named: dictionary[kIMAGENAME] as? String ?? "")
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(idCategory)
    }

    static func == (lhs: Category, rhs: Category) -> Bool {
        return lhs.idCategory == rhs.idCategory
    }

    func contains(filter: String?) -> Bool {
        guard let filter = filter else { return true }
        if filter.isEmpty { return true }
        let lowercasedFilter = filter.lowercased()
        return name?.lowercased().contains(lowercasedFilter) ?? false
    }
}
