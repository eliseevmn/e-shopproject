import Foundation
import FirebaseAuth

class MUser {
    let objetcId: String
    var email: String
    var firstName: String
    var lastName: String
    var fullName: String
    var purchasedItemIds: [String]

    var fullAddress: String?
    var onBoard: Bool

    // MARK: - Initializers
    init(objetcId: String, email: String, firstName: String, lastName: String) {
        self.objetcId = objetcId
        self.email = email
        self.firstName = firstName
        self.lastName = lastName
        fullName = firstName + " " + lastName
        fullAddress = ""
        onBoard = false
        purchasedItemIds = []
    }

    init(dictionary: NSDictionary) {
        objetcId = dictionary[kOBJECTID] as? String ?? ""

        if let mail = dictionary[kEMAIL] {
            email = mail as? String ?? ""
        } else {
            email = ""
        }

        if let fName = dictionary[kFIRSTNAME] {
            firstName = fName as? String ?? ""
        } else {
            firstName = ""
        }

        if let lName = dictionary[kLASTNAME] {
            lastName = lName as? String ?? ""
        } else {
            lastName = ""
        }

        fullName = firstName + " " + lastName

        if let fAddress = dictionary[kFULLADRESS] {
            fullAddress = fAddress as? String
        } else {
            fullAddress = ""
        }

        if let onBoarding = dictionary[kONBOARD] {
            onBoard = onBoarding as? Bool ?? false
        } else {
            onBoard = false
        }

        if let purchaseIds = dictionary[kPURCHASEDITEMSIDS] {
            purchasedItemIds = purchaseIds as? [String] ?? []
        } else {
            purchasedItemIds = []
        }
    }

    // MARK: - Return current user
    class func currentId() -> String {
        return Auth.auth().currentUser!.uid
    }

    class func currentUser() -> MUser? {
        if Auth.auth().currentUser != nil {
            if let dictionary = UserDefaults.standard.object(forKey: kCURRENTUSER) {
                return MUser(dictionary: dictionary as? NSDictionary ?? NSDictionary())
            }
        }

        return nil
    }
}
