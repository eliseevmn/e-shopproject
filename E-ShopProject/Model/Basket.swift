import Foundation

class Basket {
    var idBasket: String
    var ownerId: String
    var itemIds: [String]

    init(idBasket: String, ownerId: String, itemIds: [String]) {
        self.idBasket = idBasket
        self.ownerId = ownerId
        self.itemIds = itemIds
    }

    init(dictionary: NSDictionary) {
        idBasket = dictionary[kOBJECTID] as? String ?? ""
        ownerId = dictionary[kOWNERID] as? String ?? ""
        itemIds = dictionary[kITEMSIDS] as? [String] ?? []
    }
}
