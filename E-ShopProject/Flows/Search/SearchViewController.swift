import Foundation
import NVActivityIndicatorView
import EmptyDataSet_Swift

class SearchViewController: UIViewController {

    // MARK: - Outlets
    private var searchBarButton: UIBarButtonItem?
    private var tableView: UITableView!
    private let searchView: UIView = {
        let view = UIView()
        return view
    }()
    private let searchTextField = CustomEditTextField(cornerRadius: 4, height: 50, fontSize: 15, placeholderText: "Search...")
    private let searchButton = UIButton(title: "Search", isShadow: true, isBorder: false)

    // MARK: - Variables
    private var activityIndicator: NVActivityIndicatorView?
    private var viewModel: SearchViewModelProtocol = SearchViewModel()

    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Search"

        setupTableView()
        setupUI()
        setupBarButtonItems()

        searchTextField.addTarget(self, action: #selector(textFieldDidChenge), for: .editingChanged)
        searchButton.addTarget(self, action: #selector(searchButtonPressed), for: .touchUpInside)

        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        activityIndicator = NVActivityIndicatorView(
            frame: CGRect(x: self.view.frame.width/2 - 30,
                          y: self.view.frame.height/2 - 30,
                          width: 60, height: 60),
            type: .ballPulse,
            color: #colorLiteral(red: 0.9098039216, green: 0.2705882353, blue: 0.3529411765, alpha: 1),
            padding: nil)
    }

    // MARK: - Actions
    @objc private func handleShowSearchBarButtonTapped() {
        dismissKeyBoard()
        showSearchField()
    }

    @objc private func searchButtonPressed() {
        guard let searchTextfield = searchTextField.text else {
            return
        }

        if searchTextfield != "" {
            searchInFirebase(forName: searchTextfield)
            emptyTextField()
            animateSearchOptionIn()
            dismissKeyBoard()
        }
    }

    @objc private func textFieldDidChenge(texfield: UITextField) {
        searchButton.isEnabled = texfield.text != ""

        if searchButton.isEnabled {
            searchButton.backgroundColor = #colorLiteral(red: 0.9098039216, green: 0.2705882353, blue: 0.3529411765, alpha: 1)
        } else {
            disableSearchButton()
        }
    }

    // MARK: - Search database
    private func searchInFirebase(forName: String) {
        showLoadingIndicator()

        viewModel.searchInFirebase(forName: forName) {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            self.hideLoadingIndicator()
        }
    }

    // MARK: - Animations
    private func animateSearchOptionIn() {
        UIView.animate(withDuration: 0.5) {
            self.searchView.isHidden = !self.searchView.isHidden
        }
    }

    // MARK: - ActivityIndicator
    private func showLoadingIndicator() {
        guard let activityIndicator = activityIndicator else {
            return
        }
        self.view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
    }

    private func hideLoadingIndicator() {
        guard let activityIndicator = activityIndicator else {
            return
        }
        activityIndicator.removeFromSuperview()
        activityIndicator.stopAnimating()
    }

    // MARK: - Helpers
    private func emptyTextField() {
        searchTextField.text = ""
    }

    private func dismissKeyBoard() {
        self.view.endEditing(false)
    }

    private func disableSearchButton() {
        searchButton.isEnabled = false
        searchButton.backgroundColor = .gray
    }

    private func showSearchField() {
        disableSearchButton()
        emptyTextField()
        animateSearchOptionIn()
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: SearchCell.reuseId,
            for: indexPath) as? SearchCell else {
                return UITableViewCell()
        }
        let cellViewModel = viewModel.cellViewModel(indexPath: indexPath)
        cell.viewModel = cellViewModel
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        viewModel.selectRow(for: indexPath)
        let itemViewModel = viewModel.viewModelForSelected()
        let controller = ItemViewController()
        controller.viewModel = itemViewModel
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

// MARK: - EmptyDataSetSource, EmptyDataSetDelegate
extension SearchViewController: EmptyDataSetSource, EmptyDataSetDelegate {

    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        return NSAttributedString(string: "No Items to Display")
    }

    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage(named: "emptyData")
    }

    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        return NSAttributedString(string: "Please add something Items")
    }

    func buttonImage(forEmptyDataSet scrollView: UIScrollView, for state: UIControl.State) -> UIImage? {
        return UIImage(named: "search")
    }

    func buttonTitle(forEmptyDataSet scrollView: UIScrollView, for state: UIControl.State) -> NSAttributedString? {
        return NSAttributedString(string: "Start searching")
    }

    func emptyDataSet(_ scrollView: UIScrollView, didTapButton button: UIButton) {
        showSearchField()
    }
}

// MARK: - Setup UI
extension SearchViewController {
    private func setupUI() {
        let searchStackView = UIStackView(arrangedSubviews: [
            searchTextField, searchButton
        ])
        searchButton.backgroundColor = .gray
        searchStackView.axis = .vertical
        searchStackView.spacing = 10
        searchView.addSubview(searchStackView)

        searchStackView.snp.makeConstraints { (make) in
            make.edges.equalTo(searchView).inset(UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20))
        }
        view.addSubview(searchView)
        searchView.isHidden = true

        let verticalStackView = UIStackView(arrangedSubviews: [
            searchView, tableView
        ])
        verticalStackView.axis = .vertical
        verticalStackView.spacing = 5
        view.addSubview(verticalStackView)
        verticalStackView.snp.makeConstraints { (make) in
            make.top.equalTo(view.safeAreaLayoutGuide)
            make.left.equalTo(view)
            make.right.equalTo(view)
            make.bottom.equalTo(view.safeAreaLayoutGuide)
        }
    }

    private func setupTableView() {
        tableView = UITableView(frame: .zero, style: .grouped)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .white
        tableView.tableFooterView = UIView()
        tableView.register(SearchCell.self, forCellReuseIdentifier: SearchCell.reuseId)
    }

    private func setupBarButtonItems() {
        searchBarButton = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(handleShowSearchBarButtonTapped))
        navigationItem.rightBarButtonItem = searchBarButton
    }
}
