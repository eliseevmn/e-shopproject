import Foundation

class SearchViewModel: SearchViewModelProtocol {

    // MARK: - Properties
    private(set)var searchResult: [Item] = []
    private var indexPath: IndexPath?

    // MARK: - Search function from firebase
    func searchInFirebase(forName: String, completion: @escaping() -> Void) {
        AlgoliaService.shared.searchAlgolia(searchString: forName) { (itemIds) in
            FirestoryServiceItems.downloadItems(withIds: itemIds) { (allItems) in

                self.searchResult = allItems
                completion()
            }
        }
    }

    // MARK: - Cell functions
    func cellViewModel(indexPath: IndexPath) -> SearchCellViewModelProtocol? {
        let item = searchResult[indexPath.row]
        return SearchCellViewModel(item: item)
    }

    func selectRow(for indexPath: IndexPath) {
        self.indexPath = indexPath
    }

    func numberOfRows() -> Int {
        return searchResult.count
    }

    func viewModelForSelected() -> ItemViewModelProtocol? {
        guard let indexPath = indexPath else { return nil }
        let item = searchResult[indexPath.row]
        return ItemViewModel(item: item)
    }
}
