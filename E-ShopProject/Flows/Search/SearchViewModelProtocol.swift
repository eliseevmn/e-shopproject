import Foundation

protocol SearchViewModelProtocol {
    func searchInFirebase(forName: String, completion: @escaping() -> Void)
    func numberOfRows() -> Int
    func viewModelForSelected() -> ItemViewModelProtocol?
    func selectRow(for indexPath: IndexPath)
    func cellViewModel(indexPath: IndexPath) -> SearchCellViewModelProtocol?
}
