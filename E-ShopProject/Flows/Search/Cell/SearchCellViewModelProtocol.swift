import Foundation

protocol SearchCellViewModelProtocol {
    var item: Item { get }
    var name: String? { get }
    var description: String? { get }
    var price: String? { get }
    var image: UIImage? { get }
    init(item: Item)
}
