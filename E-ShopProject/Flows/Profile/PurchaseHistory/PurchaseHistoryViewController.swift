import UIKit

class PurchaseHistoryViewController: UIViewController {

    // MARK: - Outlets
    private lazy var collectionView: UICollectionView = {
           let layout = UICollectionViewFlowLayout()
           layout.scrollDirection = .vertical

           let width = UIScreen.main.bounds.width
           let height = UIScreen.main.bounds.height / 10
           layout.itemSize = CGSize(width: width, height: height)

           layout.minimumInteritemSpacing = 15
           layout.minimumLineSpacing = 15

           return UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
    }()

    // MARK: - Variabales
    private var viewModel: PurchaseHistoryViewModelProtocol = PurchaseHistoryViewModel()

    // MARK: - View lyfecycles
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Purchased Items"
        setupCollectionView()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadItems()
    }

    // MARK: - Download items
    private func loadItems() {
        viewModel.loadItems(completion: {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        })
    }
}

// MARK: - UICollectionViewDelegate, UICollectionViewDataSource
extension PurchaseHistoryViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: ItemsCollectionViewCell.reuseId,
            for: indexPath) as? ItemsCollectionViewCell else {
            return UICollectionViewCell()
        }
        let cellViewModel = viewModel.cellViewModel(indexPath: indexPath)
        cell.viewModel = cellViewModel
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        viewModel.selectRow(for: indexPath)
        let itemViewModel = viewModel.viewModelForSelected()
        let controller = ItemViewController()
        controller.viewModel = itemViewModel
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

// MARK: - SetupUI
extension PurchaseHistoryViewController {
    private func setupCollectionView() {
        collectionView.backgroundColor = .white
        view.addSubview(collectionView)

        collectionView.snp.makeConstraints { (make) in
            make.edges.equalTo(view)
        }
        collectionView.delegate = self
        collectionView.dataSource = self

        collectionView.register(ItemsCollectionViewCell.self, forCellWithReuseIdentifier: ItemsCollectionViewCell.reuseId)
    }
}
