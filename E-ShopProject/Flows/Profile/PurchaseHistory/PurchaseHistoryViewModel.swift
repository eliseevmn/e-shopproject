import Foundation

class PurchaseHistoryViewModel: PurchaseHistoryViewModelProtocol {

    // MARK: - Properties
    private(set)var itemArray: [Item] = []
    private var indexPath: IndexPath?

    // MARK: - Load items from firebase
    func loadItems(completion: @escaping() -> Void) {
        FirestoryServiceItems.downloadItems(withIds: MUser.currentUser()!.purchasedItemIds) { (allItems) in
            self.itemArray = allItems
            completion()
        }
    }

    // MARK: - Cell functions
    func cellViewModel(indexPath: IndexPath) -> ItemsCellViewModelProtocol? {
        let item = itemArray[indexPath.row]
        return ItemsCellViewModel(item: item)
    }

    func numberOfRows() -> Int {
        return itemArray.count
    }

    func selectRow(for indexPath: IndexPath) {
        self.indexPath = indexPath
    }

    func viewModelForSelected() -> ItemViewModelProtocol? {
        guard let indexPath = indexPath else { return nil }
        let item = itemArray[indexPath.row]
        return ItemViewModel(item: item)
    }
}
