import Foundation

protocol PurchaseHistoryViewModelProtocol {
    func cellViewModel(indexPath: IndexPath) -> ItemsCellViewModelProtocol?
    func loadItems(completion: @escaping() -> Void)
    func numberOfRows() -> Int
    func selectRow(for indexPath: IndexPath)
    func viewModelForSelected() -> ItemViewModelProtocol?
}
