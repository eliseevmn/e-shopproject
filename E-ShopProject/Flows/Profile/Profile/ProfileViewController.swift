import UIKit

class ProfileViewController: UIViewController {

    // MARK: - Outlets
    private var finishRegistrationButton: CustomProfileButton = {
        let button = CustomProfileButton(type: .system)
        button.setTitle("Finish registration", for: .normal)
        return button
    }()
    private var purchaseHistoryButton: CustomProfileButton = {
        let button = CustomProfileButton(type: .system)
        button.setTitle("Purchase history", for: .normal)
        return button
    }()
    private var termsConditionsButton: CustomProfileButton = {
        let button = CustomProfileButton(type: .system)
        button.setTitle("Terms & Conditions", for: .normal)
        return button
    }()

    // MARK: - Variables
    var editBarButtonOutlet: UIBarButtonItem?

    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        title = "Profile"
        setupUI()

        finishRegistrationButton.addTarget(self, action: #selector(handleFinishRegistrationButtonTapped), for: .touchUpInside)
        purchaseHistoryButton.addTarget(self, action: #selector(handlePurchaseHistoryButtonTapped), for: .touchUpInside)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkLoginStatus()
        checkOnBoardingStatus()
    }

    // MARK: - Helpers
    private func checkOnBoardingStatus() {
        if MUser.currentUser() != nil {
            if MUser.currentUser()!.onBoard {
                finishRegistrationButton.setTitle("Account is active", for: .normal)
                finishRegistrationButton.isEnabled = false
            } else {
                finishRegistrationButton.setTitle("Finish registration", for: .normal)
                finishRegistrationButton.isEnabled = true
                finishRegistrationButton.tintColor = .red
            }
            purchaseHistoryButton.isEnabled = true
        } else {
            finishRegistrationButton.setTitle("Logged out", for: .normal)
            finishRegistrationButton.isEnabled = false
            purchaseHistoryButton.isEnabled = false
        }
    }

    private func checkLoginStatus() {
        if MUser.currentUser() == nil {
            createRightBarButton(title: "Login")
        } else {
            createRightBarButton(title: "Edit")
        }
    }

    private func createRightBarButton(title: String) {
        editBarButtonOutlet = UIBarButtonItem(title: title, style: .plain, target: self, action: #selector(editBarButtonTapped))
        self.navigationItem.rightBarButtonItem = editBarButtonOutlet
    }

    // MARK: - Actions
    @objc private func editBarButtonTapped() {
        if editBarButtonOutlet?.title == "Login" {
            showRegistrationView()
        } else {
            goToEditProfile()
        }
    }

    @objc private func handleFinishRegistrationButtonTapped() {
        let controller = FinishRegistrationViewController()
        self.navigationController?.pushViewController(controller, animated: true)
    }

    @objc private func handlePurchaseHistoryButtonTapped() {
        let controller = PurchaseHistoryViewController()
        self.navigationController?.pushViewController(controller, animated: true)
    }

    // MARK: - Navigation
    private func showRegistrationView() {
        let loginView = RegistrationViewController()
        self.present(loginView, animated: true, completion: nil)
    }

    private func goToEditProfile() {
        let editView = EditViewController()
        self.navigationController?.pushViewController(editView, animated: true)
    }
}

// MARK: - SetupUI
extension ProfileViewController {
    private func setupUI() {

        let stackView = UIStackView(arrangedSubviews: [
            finishRegistrationButton,
            purchaseHistoryButton,
            termsConditionsButton
        ])
        stackView.axis = .vertical
        stackView.spacing = 5
        view.addSubview(stackView)
        stackView.snp.makeConstraints { (make) in
            make.top.equalTo(view.safeAreaLayoutGuide).offset(20)
            make.leading.equalTo(view).offset(20)
            make.trailing.equalTo(view).offset(-20)
        }
    }

    private func createpBarButtonItems(title: String) {
        editBarButtonOutlet = UIBarButtonItem(title: title, style: .plain, target: self, action: #selector(editBarButtonTapped))
    }
}
