import UIKit
import Gallery
import JGProgressHUD
import NVActivityIndicatorView

class AddItemViewController: UIViewController {

    // MARK: - Outlets
    private let titleTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Введите название продукта"
        return textField
    }()
    private let priceTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Введите цену"
        return textField
    }()
    private let descriptionTextView: UITextView = {
        let textView = UITextView()
        textView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        textView.text = "Описание товара"
        textView.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        return textView
    }()
    private let cameraButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Camera", for: .normal)
        button.addTarget(self, action: #selector(handleCameraButtonTapped), for: .touchUpInside)
        return button
    }()

    // MARK: - Variables
    var viewModel: AddItemViewProtocol!
    private var gallery: GalleryController!
    private let hud = JGProgressHUD(style: .dark)
    private var activityIndicator: NVActivityIndicatorView?

    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Добавить товар"
        view.backgroundColor = .white
        setupUI()
        setupBarButtonItems()

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(backgroundTapped))
        view.addGestureRecognizer(tapGestureRecognizer)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        activityIndicator = NVActivityIndicatorView(
            frame: CGRect(x: self.view.frame.width/2 - 30,
                          y: self.view.frame.height/2 - 30,
                          width: 60, height: 60),
            type: .ballPulse,
            color: #colorLiteral(red: 0.9098039216, green: 0.2705882353, blue: 0.3529411765, alpha: 1),
            padding: nil)
    }

    // MARK: - Actions
    @objc private func handleCameraButtonTapped() {
        viewModel.emptyImageArray()
        showImageGallery()
    }

    @objc private func handleDoneBarButtonTapped() {
        dismissKeyboard()

        if fieldsAreCompleted() {
            saveToFireBase()
        } else {
            setupErrorHUD(text: "Не все поля заполнены!")
        }
    }

    @objc private func backgroundTapped() {
        dismissKeyboard()
    }

    // MARK: - Save Item
    private func saveToFireBase() {
        showLoadingIndicator()

        viewModel.saveToFireBase(itemName: titleTextField.text, itemDescription: descriptionTextView.text, priceDescription: priceTextField.text) {
            if self.viewModel.itemImages.count > 0 {
                self.hideLoadingIndicator()
                self.dismissController()
            } else {
                self.dismissController()
            }
        }
    }

    // MARK: - Helper functions
    private func fieldsAreCompleted() -> Bool {
        return (titleTextField.text != "" && priceTextField.text != "" && descriptionTextView.text != "")
    }

    private func dismissKeyboard() {
        self.view.endEditing(true)
    }

    private func dismissController() {
        self.navigationController?.popViewController(animated: true)
    }

    // MARK: - Activity Indicator
    private func showLoadingIndicator() {
        guard let activityIndicator = activityIndicator else { return }
        self.view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
    }

    private func hideLoadingIndicator() {
        guard let activityIndicator = activityIndicator else { return }
        activityIndicator.removeFromSuperview()
        activityIndicator.stopAnimating()
    }

    // MARK: - JGProgressHUD
    private func setupErrorHUD(text: String) {
        self.hud.textLabel.text = text
        self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
        self.hud.show(in: self.view, animated: true)
        self.hud.dismiss(afterDelay: 2)
    }

    // MARK: - Show gallery
    private func showImageGallery() {
        self.gallery = GalleryController()
        self.gallery.delegate = self
        Config.tabsToShow = [.imageTab, .cameraTab]
        Config.Camera.imageLimit = 6
        self.present(gallery, animated: true, completion: nil)
    }
}

// MARK: - GalleryControllerDelegate
extension AddItemViewController: GalleryControllerDelegate {
    func galleryController(_ controller: GalleryController, didSelectImages images: [Image]) {

        if images.count > 0 {
            Image.resolve(images: images) { (resolvedImages) in
                resolvedImages.forEach { (image) in
                    guard let image = image else { return }
                    self.viewModel.itemImages.append(image)
                }
            }
        }
        controller.dismiss(animated: true, completion: nil)
    }

    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {
        controller.dismiss(animated: true, completion: nil)
    }

    func galleryController(_ controller: GalleryController, requestLightbox images: [Image]) {
        controller.dismiss(animated: true, completion: nil)
    }

    func galleryControllerDidCancel(_ controller: GalleryController) {
        controller.dismiss(animated: true, completion: nil)
    }
}

// MARK: - Setup UI
extension AddItemViewController {
    private func setupUI() {
        let stackView = UIStackView(arrangedSubviews: [
            titleTextField,
            priceTextField,
            descriptionTextView
        ])
        stackView.axis = .vertical
        stackView.spacing = 30

        view.addSubview(stackView)
        view.addSubview(cameraButton)

        stackView.snp.makeConstraints { (make) in
            make.top.equalTo(view.safeAreaLayoutGuide).offset(30)
            make.leading.equalTo(view).offset(20)
            make.trailing.equalTo(view).offset(-20)
        }

        cameraButton.snp.makeConstraints { (make) in
            make.top.equalTo(stackView.snp.bottom).offset(30)
            make.leading.equalTo(view).offset(20)
        }
    }

    private func setupBarButtonItems() {
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(handleDoneBarButtonTapped))
        navigationItem.rightBarButtonItem = doneBarButton
    }
}
