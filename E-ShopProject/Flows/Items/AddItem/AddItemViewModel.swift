import Foundation

class AddItemViewModel: AddItemViewProtocol {

    // MARK: - Properties
    private var category: Category?
    var itemImages: [UIImage] = []

    // MARK: - Save to firebase item
    func saveToFireBase(itemName: String?, itemDescription: String?, priceDescription: String?, completion: @escaping() -> Void) {
        let itemId = UUID().uuidString
        let itemName = itemName
        let categoryId = category?.idCategory
        let itemDescription = itemDescription
        let itemPrice = Double(priceDescription ?? "0.0")

        let item = Item(idItem: itemId,
                        categoryId: categoryId ?? "",
                        name: itemName ?? "",
                        description: itemDescription ?? "",
                        price: itemPrice ?? 0.0,
                        imageLinks: [])

        if itemImages.count > 0 {
            ImageService.uploadImages(images: itemImages, itemId: itemId) { (imageLinkArray) in
                item.imageLinks = imageLinkArray
                FirestoryServiceItems.saveItemsToFirestore(item: item)
                FirestoryServiceItems.saveItemsToAlgolia(item: item)
                completion()
            }
        } else {
            FirestoryServiceItems.saveItemsToFirestore(item: item)
            FirestoryServiceItems.saveItemsToAlgolia(item: item)
            completion()
        }
    }

    // MARK: - Helper functions
    func emptyImageArray() {
        itemImages = []
    }

    // MARK: - Init
    required init(category: Category) {
        self.category = category
    }
}
