import Foundation

protocol AddItemViewProtocol {
    var itemImages: [UIImage] { get set }
    func emptyImageArray()
    func saveToFireBase(itemName: String?, itemDescription: String?, priceDescription: String?, completion: @escaping() -> Void)
    init(category: Category)
}
