import UIKit
import JGProgressHUD

class ItemViewController: UIViewController {

    // MARK: - Outlets
    private var collectionView: UICollectionView!
    private let nameLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    private let titlePriceLabel: UILabel = {
        let label = UILabel()
        label.text = "Price"
        label.widthAnchor.constraint(equalToConstant: 80).isActive = true
        return label
    }()
    private let priceLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        return label
    }()
    private let titleDescritionLabel: UILabel = {
        let label = UILabel()
        label.text = "Описание"
        label.font = .boldSystemFont(ofSize: 16)
        return label
    }()
    private let descriprionLabel: UILabel = {
        let label = UILabel()
        return label
    }()

    // MARK: - Variables
    let hud = JGProgressHUD(style: .dark)
    private let sectionInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    private let cellHeight: CGFloat = 196
    private let itemsPerRow: CGFloat = 1
    var viewModel: ItemViewModelProtocol?

    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        setupCollectionView()
        setupUI()
        setupBarButtonItems()
        setupItem()

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        downloadPictures()
    }

    // MARK: - Actions
    @objc private func backAction() {
        self.navigationController?.popViewController(animated: true)
    }

    @objc private func addToBasketButtonPressed() {
        if MUser.currentUser() != nil {
            viewModel?.addToBasketButtonPressed(completion: { (error) in
                if error != nil {
                    self.hudError(text: error?.localizedDescription ?? "Error adding to the basket")
                } else {
                    self.hudSuccess(text: "Added to basket")
                }
            })
        } else {
            showLoginView()
        }
    }

    // MARK: - Download picture
    private func downloadPictures() {
        viewModel?.downloadPictures(completion: {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        })
    }

    // MARK: - Setup Item
    private func setupItem() {
        viewModel?.setupItem(completion: { item in
            self.title = item?.name
            self.nameLabel.text = item?.name
            self.priceLabel.text = HelperFunctions.convertToCurrency(item?.price ?? 0.0)
            self.descriprionLabel.text = item?.description
        })
    }

    // MARK: - Helpers
    private func hudSuccess(text: String) {
        self.hud.textLabel.text = text
        self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
        self.hud.show(in: self.view)
        self.hud.dismiss(afterDelay: 2.0)
    }

    private func hudError(text: String) {
        self.hud.textLabel.text = text
        self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
        self.hud.show(in: self.view)
        self.hud.dismiss(afterDelay: 2.0)
    }

    // MARK: - Navigation
    private func showLoginView() {
        let registrationVC = RegistrationViewController()
        self.present(registrationVC, animated: true, completion: nil)
    }
}

// MARK: - UICollectionViewDelegate, UICollectionViewDataSource
extension ItemViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel?.numberOfRows() ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: ImageCollectionViewCell.reuseId,
            for: indexPath) as? ImageCollectionViewCell else {
            return UICollectionViewCell()
        }
        let cellViewModel = viewModel?.cellViewModel(indexPath: indexPath)
        cell.viewModel = cellViewModel
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension ItemViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let avaibleWidth = collectionView.frame.width - sectionInsets.left
        return CGSize(width: avaibleWidth, height: cellHeight)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}

// MARK: - Setup UI
extension ItemViewController {
    private func setupUI() {
        view.addSubview(collectionView)

        collectionView.snp.makeConstraints { (make) in
            make.top.equalTo(view.safeAreaLayoutGuide)
            make.leading.equalTo(view)
            make.trailing.equalTo(view)
            make.height.equalTo(200)
        }

        let stackView = UIStackView(arrangedSubviews: [
            titlePriceLabel, priceLabel
        ])
        stackView.spacing = 10

        let overlayStackView = UIStackView(arrangedSubviews: [
            nameLabel, stackView, titleDescritionLabel, descriprionLabel
        ])
        overlayStackView.axis = .vertical
        overlayStackView.spacing = 20

        view.addSubview(overlayStackView)
        overlayStackView.snp.makeConstraints { (make) in
            make.top.equalTo(collectionView.snp.bottom).offset(20)
            make.leading.equalTo(view).offset(20)
            make.trailing.equalTo(view).offset(-20)
        }
    }

    private func setupBarButtonItems() {
        self.navigationItem.leftBarButtonItems = [
            UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(backAction))]
        self.navigationItem.rightBarButtonItems = [
            UIBarButtonItem(image: #imageLiteral(resourceName: "addToBasket"), style: .plain, target: self, action: #selector(addToBasketButtonPressed))]
    }

    private func setupCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal

        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.collectionViewLayout = layout

        collectionView.backgroundColor = .white
        collectionView.dataSource = self
        collectionView.delegate = self

        collectionView.register(ImageCollectionViewCell.self, forCellWithReuseIdentifier: ImageCollectionViewCell.reuseId)
    }
}
