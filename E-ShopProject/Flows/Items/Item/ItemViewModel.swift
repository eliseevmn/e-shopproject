import Foundation

class ItemViewModel: ItemViewModelProtocol {

    // MARK: - Properties
    private var item: Item?
    private var itemImages: [UIImage] = []

    // MARK: - Init
    required init(item: Item) {
        self.item = item
    }

    // MARK: - Firebase functions with basket
    func addToBasketButtonPressed(completion: @escaping(FirebaseError?) -> Void) {
        FirestoreServiceBasket.downloadBasketFromFirestore(ownerId: MUser.currentId()) { (basket) in
            if basket == nil {
                self.createNewBasket(completion: completion)
            } else {
                basket!.itemIds.append(self.item!.idItem)
                self.updateBasket(basket: basket!, withValues: [kITEMSIDS: basket!.itemIds], completion: completion)
            }
        }
    }

    private func createNewBasket(completion: @escaping(FirebaseError?) -> Void) {
        let itemId = self.item?.idItem ?? ""
        let basketId = UUID().uuidString
        let ownerId = MUser.currentId()
        let itemIds = [itemId]

        let newBasket = Basket(idBasket: basketId, ownerId: ownerId, itemIds: itemIds)
        FirestoreServiceBasket.saveBasketToFirestore(basket: newBasket)
        completion(nil)
    }

    private func updateBasket(basket: Basket, withValues: [String: Any], completion: @escaping(FirebaseError?) -> Void) {
        FirestoreServiceBasket.updateBasketInFirestore(basket: basket, withValues: withValues) { (error) in
            if error != nil {
                completion(FirebaseError.errorUpdatingBasket)
            } else {
                completion(nil)
            }
        }
    }

    // MARK: - Download image from firebase
    func downloadPictures(completion: @escaping() -> Void) {
        if item != nil && item?.imageLinks != nil {
            ImageService.downloadImages(imageUrls: item?.imageLinks ?? []) { (allImages) in
                if allImages.count > 0 {
                    self.itemImages = allImages
                    completion()
                }
            }
        }
    }

    // MARK: - Helper functions
    func setupItem(completion: @escaping(_ item: Item?) -> Void) {
        if item != nil {
            completion(item)
        }
    }

    // MARK: - Cell functions
    func cellViewModel(indexPath: IndexPath) -> ImageCellViewModelProtocol? {
        if itemImages.count > 0 {
            let itemImage = itemImages[indexPath.item]
            return ImageCellViewModel(iconImage: itemImage)
        } else {
            return ImageCellViewModel(iconImage: #imageLiteral(resourceName: "imagePlaceholder"))
        }
    }

    func numberOfRows() -> Int {
        return itemImages.count == 0 ? 1 : itemImages.count
    }
}
