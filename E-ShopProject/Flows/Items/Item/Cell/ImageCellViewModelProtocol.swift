import Foundation

protocol ImageCellViewModelProtocol {
    var imageCell: UIImage? { get }
    init(iconImage: UIImage?)
}
