import Foundation

class ImageCellViewModel: ImageCellViewModelProtocol {

    private let image: UIImage?
    var imageCell: UIImage? {
        return image
    }
    required init(iconImage: UIImage?) {
        self.image = iconImage
    }
}
