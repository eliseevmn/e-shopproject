import UIKit

class ImageCollectionViewCell: UICollectionViewCell {

    // MARK: - Variables
    static let reuseId = "ImageCollectionViewCell"
    var viewModel: ImageCellViewModelProtocol! {
        didSet {
            imageView.image = viewModel.imageCell
        }
    }
    // MARK: - Outlets
    private var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()

    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: .zero)
        backgroundColor = .white
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        imageView.image = nil
    }

    // MARK: - Setup UI
    private func setupUI() {
        addSubview(imageView)
        imageView.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
    }
}
