import Foundation

protocol ItemViewModelProtocol {
    init(item: Item)

    func addToBasketButtonPressed(completion: @escaping(FirebaseError?) -> Void)
    func setupItem(completion: @escaping(_ item: Item?) -> Void)
    func downloadPictures(completion: @escaping() -> Void)
    func numberOfRows() -> Int
    func cellViewModel(indexPath: IndexPath) -> ImageCellViewModelProtocol?
}
