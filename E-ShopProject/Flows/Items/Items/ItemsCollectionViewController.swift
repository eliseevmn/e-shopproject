import UIKit

class ItemsCollectionViewController: UIViewController {

    // MARK: - Outlets
    private var collectionView: UICollectionView!

    // MARK: - Variabales
    var viewModel: ItemsViewModelProtocol!

    // MARK: - View lyfecycles
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        title = viewModel.category?.name

        setupSearchBar()
        setupCollectionView()
        setupBarButtonItems()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if viewModel.category != nil {
            loadItems()
        }
    }

    // MARK: - Download categories
    private func loadItems() {
        viewModel.loadItems {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }

    private func loadSearchItems(with searchText: String?) {
        viewModel.reloadData(with: searchText) {
            DispatchQueue.main.async {
                 self.collectionView.reloadData()
             }
        }
    }

    // MARK: - Reload data
    private func reloadData(with searchText: String?) {
        if searchText != "" {
            loadSearchItems(with: searchText)
        } else {
            loadItems()
        }
    }

    // MARK: - Actions
    @objc private func handleAddBarButtonTapped() {
        let addItemController = AddItemViewController()
        addItemController.viewModel = viewModel.viewModelToAddController()
        self.navigationController?.pushViewController(addItemController, animated: true)
    }
}

// MARK: - UICollectionViewDataSource
extension ItemsCollectionViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: ItemsCollectionViewCell.reuseId,
            for: indexPath) as? ItemsCollectionViewCell else {
            return UICollectionViewCell()
        }
        let cellViewModel = viewModel.cellViewModel(indexPath: indexPath)
        cell.viewModel = cellViewModel
        return cell
    }
}

// MARK: - UICollectionViewDelegate
extension ItemsCollectionViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.selectRow(for: indexPath)
        let itemViewModel = viewModel.viewModelForSelected()
        let controller = ItemViewController()
        controller.viewModel = itemViewModel
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

// MARK: - UISearchBarDelegate
extension ItemsCollectionViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        reloadData(with: searchText)
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension ItemsCollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width, height: 80)
    }
}
// MARK: - SetupUI
extension ItemsCollectionViewController {
    private func setupCollectionView() {
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        collectionView.backgroundColor = .white
        view.addSubview(collectionView)

        collectionView.snp.makeConstraints { (make) in
            make.top.equalTo(view.safeAreaLayoutGuide)
            make.leading.equalTo(view)
            make.bottom.equalTo(view.safeAreaLayoutGuide)
            make.trailing.equalTo(view)
        }

        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(ItemsCollectionViewCell.self, forCellWithReuseIdentifier: ItemsCollectionViewCell.reuseId)
    }

    private func setupSearchBar() {
        navigationController?.navigationBar.barTintColor = .mainWhite()
        navigationController?.navigationBar.shadowImage = UIImage()
        let searchController = UISearchController(searchResultsController: nil)
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
    }

    private func setupBarButtonItems() {
        let addBarButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(handleAddBarButtonTapped))
        navigationItem.rightBarButtonItem = addBarButton
    }
}
