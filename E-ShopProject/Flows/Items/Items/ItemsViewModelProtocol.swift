import Foundation

protocol ItemsViewModelProtocol {
    var category: Category? { get }
    var itemArray: [Item] { get }

    func loadItems(completion: @escaping() -> Void)
    func reloadData(with searchText: String?, completion: @escaping () -> Void)

    func selectRow(for indexPath: IndexPath)
    func cellViewModel(indexPath: IndexPath) -> ItemsCellViewModelProtocol?
    func numberOfRows() -> Int
    func viewModelForSelected() -> ItemViewModelProtocol?
    func viewModelToAddController() -> AddItemViewProtocol?

    init(category: Category)
}
