import UIKit

class ItemsCellViewModel: ItemsCellViewModelProtocol {

    var item: Item

    var name: String? {
        return item.name
    }

    var description: String? {
        return item.description
    }

    var price: String? {
        return setupPrice(price: item.price)
    }

    private func setupPrice(price: Double) -> String? {
        let priceText = HelperFunctions.convertToCurrency(item.price)
        return priceText
    }

    required init(item: Item) {
        self.item = item
    }
}
