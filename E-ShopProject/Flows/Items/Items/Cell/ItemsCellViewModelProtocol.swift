import Foundation

protocol ItemsCellViewModelProtocol {
    var item: Item { get }
    var name: String? { get }
    var description: String? { get }
    var price: String? { get }
    init(item: Item)
}
