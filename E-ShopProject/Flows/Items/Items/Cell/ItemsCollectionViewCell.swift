import UIKit

class ItemsCollectionViewCell: UICollectionViewCell {

    // MARK: - Variables
    static let reuseId = "ItemsCollectionViewCell"
    var viewModel: ItemsCellViewModelProtocol! {
        didSet {
            nameLabel.text = viewModel.name
            descriptionLabel.text = viewModel.description
            priceLabel.text = viewModel.price
            generateImageInCell(item: viewModel.item)
        }
    }

    // MARK: - Outlets
    private var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.layer.cornerRadius = 30
        imageView.layer.masksToBounds = true
        imageView.heightAnchor.constraint(equalToConstant: 60).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 60).isActive = true
        return imageView
    }()
    private var nameLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.textAlignment = .center
        return label
    }()
    private var descriptionLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.numberOfLines = 0
        return label
    }()
    private var priceLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.textAlignment = .right
        return label
    }()

    // MARk: - Init
    override init(frame: CGRect) {
        super.init(frame: .zero)
        backgroundColor = .white
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        imageView.image = nil
    }

    // MARK: - Setup UI
    private func setupUI() {
        let informationStackView = UIStackView(arrangedSubviews: [
            nameLabel, descriptionLabel
        ])
        informationStackView.axis = .vertical
        informationStackView.spacing = 10
        informationStackView.alignment = .leading

        let stackView = UIStackView(arrangedSubviews: [
            imageView, informationStackView, priceLabel
        ])
        stackView.spacing = 10
        stackView.alignment = .center

        addSubview(stackView)
        stackView.snp.makeConstraints { (make) in
            make.edges.equalTo(self).inset(UIEdgeInsets(top: 5, left: 16, bottom: 5, right: 16))
        }

        self.layer.cornerRadius = 4

        self.layer.shadowColor = #colorLiteral(red: 0.7411764706, green: 0.7411764706, blue: 0.7411764706, alpha: 1)
        self.layer.shadowRadius = 3
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 0, height: 4)
    }

    // MARK: - Generate Cell
    func generateImageInCell(item: Item) {
        if item.imageLinks.count > 0 {
            ImageService.downloadImages(imageUrls: [item.imageLinks.first!]) { (images) in
                self.imageView.image = images.first
            }
        } else {
            self.imageView.image = #imageLiteral(resourceName: "imagePlaceholder")
        }
    }
}
