import Foundation

class ItemsViewModel: ItemsViewModelProtocol {

    // MARK: - Properties
    private(set)var category: Category?
    private(set)var itemArray: [Item] = []
    private var indexPath: IndexPath?

    // MARK: - Load items from firebase
    func loadItems(completion: @escaping () -> Void) {
        FirestoryServiceItems.downloadItemsFromFirestore(withCategoryId: category!.idCategory!) { (allItems) in
            self.itemArray = allItems
            completion()
        }
    }

    // MARK: - Filter items
    func reloadData(with searchText: String?, completion: @escaping () -> Void) {
        let filtered = itemArray.filter { (item) -> Bool in
            item.contains(filter: searchText)
        }
        self.itemArray = filtered
        completion()
    }

    // MARK: - Cell funcstions
    func cellViewModel(indexPath: IndexPath) -> ItemsCellViewModelProtocol? {
        let item = itemArray[indexPath.row]
        return ItemsCellViewModel(item: item)
    }

    func viewModelForSelected() -> ItemViewModelProtocol? {
        guard let indexPath = indexPath else { return nil }
        let item = itemArray[indexPath.row]
        return ItemViewModel(item: item)
    }

    func viewModelToAddController() -> AddItemViewProtocol? {
        guard let category = category else { return nil }
        return AddItemViewModel(category: category)
    }

    func selectRow(for indexPath: IndexPath) {
        self.indexPath = indexPath
    }

    func numberOfRows() -> Int {
        return itemArray.count
    }

    // MARK: - Init
    required init(category: Category) {
        self.category = category
    }
}
