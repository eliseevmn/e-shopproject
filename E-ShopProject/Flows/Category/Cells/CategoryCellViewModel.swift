import Foundation

class CategoryCellViewModel: CategoryCellViewModelProtocol {

    private let category: Category
    var name: String? {
        return category.name
    }
    var image: UIImage? {
        return category.image
    }
    required init(category: Category) {
        self.category = category
    }
}
