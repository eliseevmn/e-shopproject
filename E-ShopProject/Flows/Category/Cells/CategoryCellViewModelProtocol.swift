import Foundation

protocol CategoryCellViewModelProtocol {
    var name: String? { get }
    var image: UIImage? { get }
    init(category: Category)
}
