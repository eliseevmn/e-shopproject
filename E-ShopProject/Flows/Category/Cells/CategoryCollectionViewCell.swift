import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {

    // MARK: - Variables
    static let reuseId = "CategoryCollectionViewCell"
    var viewModel: CategoryCellViewModelProtocol! {
        didSet {
            nameLabel.text = viewModel.name
            imageView.image = viewModel.image
        }
    }

    // MARK: - Outlets
    private var nameLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    private var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    // MARk: - Init
    override init(frame: CGRect) {
        super.init(frame: .zero)
        backgroundColor = .white
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        imageView.image = nil
    }

    // MARK: - Setup UI
    private func setupUI() {
        let stackView = UIStackView(arrangedSubviews: [
            nameLabel, imageView
        ])
        stackView.axis = .vertical
        stackView.spacing = 10

        addSubview(stackView)
        stackView.snp.makeConstraints { (make) in
            make.edges.equalTo(self).inset(UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5))
        }

        self.layer.cornerRadius = 4
        self.layer.shadowColor = #colorLiteral(red: 0.7411764706, green: 0.7411764706, blue: 0.7411764706, alpha: 1)
        self.layer.shadowRadius = 3
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 0, height: 4)
    }
}
