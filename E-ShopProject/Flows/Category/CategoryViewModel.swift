import Foundation

class CategoryViewModel: CategoryViewModelProtocol {

    // MARK: - Properties
    private(set)var categoryArray: [Category] = []
    private var indexPath: IndexPath?

    // MARK: - Load categories from Firebaes
    func loadCategories(completion: @escaping () -> Void) {
        FirestoryServiceCategory.downloadCategoriesFromFirebase { (allCategories) in
            self.categoryArray = allCategories
            completion()
        }
    }

    // MARK: - Filter categories
    func reloadData(with searchText: String?, completion: @escaping () -> Void) {
        let filtered = categoryArray.filter { (category) -> Bool in
            category.contains(filter: searchText)
        }
        self.categoryArray = filtered
        completion()
    }

    // MARK: - Cell functions
    func cellViewModel(indexPath: IndexPath) -> CategoryCellViewModelProtocol? {
        let category = categoryArray[indexPath.row]
        return CategoryCellViewModel(category: category)
    }

    func selectRow(for indexPath: IndexPath) {
        self.indexPath = indexPath
    }

    func numberOfRows() -> Int {
        return categoryArray.count
    }

    func viewModelForSelected() -> ItemsViewModelProtocol? {
        guard let indexPath = indexPath else { return nil }
        let category = categoryArray[indexPath.row]
        return ItemsViewModel(category: category)
    }
}
