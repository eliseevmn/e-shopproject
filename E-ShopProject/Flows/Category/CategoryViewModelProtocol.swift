import Foundation

protocol CategoryViewModelProtocol {
    var categoryArray: [Category] { get }
    func loadCategories(completion: @escaping() -> Void)
    func reloadData(with searchText: String?, completion: @escaping () -> Void)
    func cellViewModel(indexPath: IndexPath) -> CategoryCellViewModelProtocol?
    func selectRow(for indexPath: IndexPath)
    func numberOfRows() -> Int
    func viewModelForSelected() -> ItemsViewModelProtocol?
}
