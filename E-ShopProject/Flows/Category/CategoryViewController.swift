import UIKit

class CategoryViewConroller: UIViewController {

    // MARK: - Outlets
    private var collectionView: UICollectionView!

    // MARK: - Variables
    private let sectionInsets = UIEdgeInsets(top: 20.0, left: 10.0, bottom: 20.0, right: 10.0)
    private let itemsPerRow: CGFloat = 3
    private var viewModel: CategoryViewModelProtocol = CategoryViewModel()

    // MARK: - View lyfecycles
    override func viewDidLoad() {
        super.viewDidLoad()

        setupSearchBar()
        setupCollectionView()
        loadCategories()
    }

    // MARK: - Download categories
    private func loadCategories() {
        viewModel.loadCategories {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }

    // MARK: - Search categories
    private func loadSearchCategories(with searchText: String?) {
        viewModel.reloadData(with: searchText) {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }

    // MARK: - Reload data
    private func reloadData(with searchText: String?) {
        if searchText != "" {
            loadSearchCategories(with: searchText)
        } else {
            loadCategories()
        }
    }
}

// MARK: - UISearchBarDelegate
extension CategoryViewConroller: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        reloadData(with: searchText)
    }
}

// MARK: - UICollectionViewDataSource
extension CategoryViewConroller: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: CategoryCollectionViewCell.reuseId,
            for: indexPath) as? CategoryCollectionViewCell else {
                return UICollectionViewCell()
        }
        let cellViewModel = viewModel.cellViewModel(indexPath: indexPath)
        cell.viewModel = cellViewModel
        return cell
    }
}

// MARK: - UICollectionViewDelegate
extension CategoryViewConroller: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.selectRow(for: indexPath)
        let itemsViewModel = viewModel.viewModelForSelected()
        let controller = ItemsCollectionViewController()
        controller.viewModel = itemsViewModel
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension CategoryViewConroller: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {

        guard let flowLayout = collectionViewLayout as? UICollectionViewFlowLayout else { return CGSize() }

        let sectionRight = flowLayout.sectionInset.right
        let sectionLeft = flowLayout.sectionInset.left
        let minimum = flowLayout.minimumInteritemSpacing * CGFloat(itemsPerRow - 1)
        let totalSpace = sectionLeft + sectionRight + minimum

        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(itemsPerRow))

        return CGSize(width: size, height: size)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}

// MARK: - SetupUI
extension CategoryViewConroller {
    private func setupCollectionView() {
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        collectionView.backgroundColor = .white
        view.addSubview(collectionView)

        collectionView.snp.makeConstraints { (make) in
            make.top.equalTo(view.safeAreaLayoutGuide)
            make.left.equalTo(view)
            make.bottom.equalTo(view.safeAreaLayoutGuide)
            make.right.equalTo(view)
        }

        collectionView.delegate = self
        collectionView.dataSource = self

        collectionView.register(CategoryCollectionViewCell.self, forCellWithReuseIdentifier: CategoryCollectionViewCell.reuseId)
    }

    private func setupSearchBar() {
        navigationController?.navigationBar.barTintColor = .mainWhite()
        navigationController?.navigationBar.shadowImage = UIImage()
        let searchController = UISearchController(searchResultsController: nil)
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
    }
}
