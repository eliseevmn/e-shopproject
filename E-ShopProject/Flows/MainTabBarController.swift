import UIKit

class MainTabBarController: UITabBarController {

    // MARK: - ViewController lyfecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        viewControllers = [
            createNavController(viewController: CategoryViewConroller(), title: "Category", imageName: "homeIcon"),
            createNavController(viewController: SearchViewController(), title: "Search", imageName: "search"),
            createNavController(viewController: BasketViewController(), title: "Корзина", imageName: "basket"),
            createNavController(viewController: ProfileViewController(), title: "Profile", imageName: "profile")
        ]
    }

    // MARK: - Private functions
    private func createNavController(viewController: UIViewController,
                                     title: String,
                                     imageName: String) -> UIViewController {
        let navController = UINavigationController(rootViewController: viewController)
        viewController.navigationItem.title = title
        viewController.view.backgroundColor = .white
        navController.tabBarItem.title = title
        navController.tabBarItem.image = UIImage(named: imageName)
        return navController
    }
}
