import Foundation
import Stripe

class BasketViewModel: BasketViewModelProtocol {

    // MARK: - Properties
    private var basket: Basket?
    private var allItems: [Item] = []
    private var totalPrice: Int = 0
    private var purchasedItemsIds: [String] = []
    private var indexPath: IndexPath?

    // MARK: - Load items from basket in firebase
    func loadBasketFromFirestore(completion: @escaping() -> Void) {
        FirestoreServiceBasket.downloadBasketFromFirestore(ownerId: MUser.currentId()) { (basket) in
            self.basket = basket
            completion()
        }
    }

    // MARK: - Helpers funstions for firebase
    func getBasketItems(comletion: @escaping(Error?) -> Void) {
        if basket != nil {
            FirestoryServiceItems.downloadItems(withIds: basket!.itemIds) { (allItems) in
                self.allItems = allItems
                comletion(nil)
            }
        }
    }

    func removeItemFromBasket(itemId: String) {
        for item in 0..<basket!.itemIds.count {
            if itemId == basket!.itemIds[item] {
                basket!.itemIds.remove(at: item)
                return
            } else {

            }
        }
    }

    func returnBasketTotalPrice() -> String {
        var totalPrice = 0.0
        for item in allItems {
            totalPrice += item.price
        }
        return "Итоговая цена: " + HelperFunctions.convertToCurrency(totalPrice)
    }

    func emptyTheBasket(completion: @escaping(Error?) -> Void) {
        purchasedItemsIds.removeAll()
        allItems.removeAll()

        completion(nil)

        basket!.itemIds = []
        FirestoreServiceBasket.updateBasketInFirestore(basket: basket!, withValues: [kITEMSIDS: basket!.itemIds]) { (error) in
            if error != nil {
                completion(error)
            } else {
                self.getBasketItems(comletion: completion)
            }
        }
    }

    func updateBasketInFirestore(completion: @escaping(Error?) -> Void) {
        FirestoreServiceBasket.updateBasketInFirestore(basket: basket!, withValues: [kITEMSIDS: basket!.itemIds]) { (error) in
            if error != nil {
                completion(error)
            }
            completion(nil)
        }
    }

    func addItemsToPurchaseHistory(itemIds: [String]) {
        if MUser.currentUser() != nil {
            let newItemIds = MUser.currentUser()!.purchasedItemIds + itemIds
            FirestoreServiceUser.updateCurrentUserInFirestore(withValues: [kPURCHASEDITEMSIDS: newItemIds]) { (error) in
                if error != nil {
                    print("Error adding purchased items: ", error!.localizedDescription)
                }
            }
        }
    }

    // MARK: - Payment function with Stripe
    func finisPayment(token: STPToken, completion: @escaping(Error?) -> Void) {
        self.totalPrice = 0

        for item in self.allItems {
            self.totalPrice += Int(item.price)
        }

        self.totalPrice *= 100

        StripeClient.shared.createAndConformPayment(token, amount: totalPrice) { (error) in
            if error == nil {

                var purchasedIds: [String] = []
                for item in self.allItems {
                    purchasedIds.append(item.idItem)
                }
                self.purchasedItemsIds = purchasedIds
                self.emptyTheBasket(completion: completion)
                self.addItemsToPurchaseHistory(itemIds: purchasedIds)
                completion(nil)
            } else {
                completion(error)
            }
        }
    }

    // MARK: - Cell functions
    func cellViewModel(indexPath: IndexPath) -> BasketCellViewModelProtocol? {
        let item = allItems[indexPath.row]
        return BasketCellViewModel(item: item)
    }

    func selectRow(for indexPath: IndexPath) {
        self.indexPath = indexPath
    }

    func viewModelForSelected() -> ItemViewModelProtocol? {
        guard let indexPath = indexPath else { return nil }
        let item = allItems[indexPath.row]
        return ItemViewModel(item: item)
    }

    func toDeleteItem(completion: @escaping(Item) -> Void) {
        guard let indexPath = indexPath else { return }
        let itemToDelete = allItems[indexPath.row]
        allItems.remove(at: indexPath.row)
        completion(itemToDelete)
    }

    func numberOfItems() -> Int {
        return allItems.count
    }
}
