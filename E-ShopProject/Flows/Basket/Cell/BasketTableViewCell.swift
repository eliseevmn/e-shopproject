import UIKit

class BasketTableViewCell: UITableViewCell {

    // MARK: - Variables
    static let reuseId = "BasketTableViewCell"
    var viewModel: BasketCellViewModelProtocol! {
        didSet {
            nameLabel.text = viewModel.name
            descriptionLabel.text = viewModel.description
            priceLabel.text = viewModel.price
            generateImageInCell(item: viewModel.item)
        }
    }
    // MARK: - Outlets

    private var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.layer.cornerRadius = 30
        imageView.layer.masksToBounds = true
        return imageView
    }()
    private var nameLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.textAlignment = .center
        return label
    }()
    private var descriptionLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.numberOfLines = 0
        return label
    }()
    private var priceLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.textAlignment = .right
        return label
    }()

    // MARk: - Init
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .white
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        iconImageView.image = nil
    }

    // MARK: - Setup UI
    private func setupUI() {
        let informationStackView = UIStackView(arrangedSubviews: [
            nameLabel, descriptionLabel
        ])
        informationStackView.axis = .vertical
        informationStackView.spacing = 10
        informationStackView.alignment = .leading

        let stackView = UIStackView(arrangedSubviews: [
            iconImageView, informationStackView, priceLabel
        ])
        stackView.spacing = 10
        stackView.alignment = .center

        addSubview(stackView)
        iconImageView.snp.makeConstraints { (make) in
            make.width.height.equalTo(60)
        }
        stackView.snp.makeConstraints { (make) in
            make.edges.equalTo(UIEdgeInsets(top: 5, left: 16, bottom: 5, right: 16))
        }
    }

    // MARK: - Generate Cell
    func generateImageInCell(item: Item) {
        if item.imageLinks.count > 0 {
            ImageService.downloadImages(imageUrls: [item.imageLinks.first!]) { (images) in
                self.iconImageView.image = images.first
            }
        } else {
            self.iconImageView.image = #imageLiteral(resourceName: "imagePlaceholder")
        }
    }
}
