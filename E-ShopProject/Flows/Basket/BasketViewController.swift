import UIKit
import JGProgressHUD
import Stripe

class BasketViewController: UIViewController {

    // MARK: - Outlets
    private var tableView: UITableView!
    private let titleBasketTotalBasketLabel = UILabel(title: "")
    private let basketTotalPriceLabel = UILabel(title: "0", alignment: .right)
    private let titleTotalItemsLabel = UILabel(title: "Количество товаров", width: 200)
    private let totalItemsLabel = UILabel(title: "Итоговая цена: $ 0", alignment: .right)
    private let checkOutButtonOutlet = UIButton(title: "CHECK OUT", isShadow: true, isBorder: false)

    // MARK: - Variables
    let hud = JGProgressHUD(style: .dark)
    var viewModel: BasketViewModelProtocol! = BasketViewModel()

    // MARK: - View Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel = BasketViewModel()

        setupTableView()
        setupUI()

        tableView.tableFooterView = UIView()
        checkOutButtonOutlet.addTarget(self, action: #selector(handleCheckOutButtonTapped), for: .touchUpInside)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if MUser.currentUser() != nil {
            loadBasketFromFirestore()
        } else {
            self.updateTotalLabels(isEmpty: true)
        }
    }

    // MARK: - Actions
    @objc private func handleCheckOutButtonTapped() {
        if MUser.currentUser()!.onBoard {
            showPaymentOptions()
        } else {
            self.showNotitfication(text: "Please complete you profile!", isError: true)
        }
    }

    // MARK: - Download Basket
    private func loadBasketFromFirestore() {
        viewModel.loadBasketFromFirestore {
            self.getBasketItems()
        }
    }

    private func getBasketItems() {
        viewModel.getBasketItems { _ in
            self.updateTotalLabels(isEmpty: false)
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }

    // MARK: - Control checkoutbutton
    private func checkoutButtonStatusUpdate() {
        checkOutButtonOutlet.isEnabled = viewModel.numberOfItems() > 0
        if checkOutButtonOutlet.isEnabled {
            checkOutButtonOutlet.backgroundColor = #colorLiteral(red: 0.9098039216, green: 0.2705882353, blue: 0.3529411765, alpha: 1)
        } else {
            disableCheckoutButton()
        }
    }

    private func disableCheckoutButton() {
        checkOutButtonOutlet.isEnabled = false
        checkOutButtonOutlet.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
    }

    private func removeItemFromBasket(itemId: String) {
        viewModel.removeItemFromBasket(itemId: itemId)
    }

    // MARK: - Helpers

    private func updateTotalLabels(isEmpty: Bool) {
        if isEmpty {
            totalItemsLabel.text = "0"
            basketTotalPriceLabel.text = returnBasketTotalPrice()
        } else {
            totalItemsLabel.text = "\(viewModel.numberOfItems())"
            basketTotalPriceLabel.text = returnBasketTotalPrice()
        }

        checkoutButtonStatusUpdate()
    }

    private func returnBasketTotalPrice() -> String {
        return viewModel.returnBasketTotalPrice()
    }

    private func emptyTheBasket() {
        viewModel.emptyTheBasket { (error) in
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            if error != nil {
                print("Error updating basket", error!.localizedDescription)
            } else {
                self.updateTotalLabels(isEmpty: true)
            }
        }
    }

    private func hudSuccess(text: String) {
        self.hud.textLabel.text = text
        self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
        self.hud.show(in: self.view)
        self.hud.dismiss(afterDelay: 2.0)
    }

    private func hudError(text: String) {
        self.hud.textLabel.text = text
        self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
        self.hud.show(in: self.view)
        self.hud.dismiss(afterDelay: 2.0)
    }

    // MARK: - Stripe
    private func finisPayment(token: STPToken) {
        viewModel.finisPayment(token: token) { (error) in
            if error == nil {
                self.emptyTheBasket()
                self.showNotitfication(text: "Payment successfull", isError: false)
            } else {
                self.showNotitfication(text: error?.localizedDescription ?? "Error", isError: true)
            }
        }
    }

    private func showNotitfication(text: String, isError: Bool) {
        if isError {
            hudError(text: text)
        } else {
            hudSuccess(text: text)
        }
    }

    private func showPaymentOptions() {
        let alertController = UIAlertController(title: "Payment Options",
                                                message: "Choose prefered payment option",
                                                preferredStyle: .actionSheet)
        let cardAction = UIAlertAction(title: "Pay with card", style: .default) { (_) in
            let cardVC = CardInfoViewController()
            cardVC.delegate = self
            self.present(cardVC, animated: true, completion: nil)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cardAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension BasketViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfItems()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: BasketTableViewCell.reuseId,
            for: indexPath) as? BasketTableViewCell else {
                return UITableViewCell()
        }
        let cellViewModel = viewModel.cellViewModel(indexPath: indexPath)
        cell.viewModel = cellViewModel
        return cell
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {viewModel.selectRow(for: indexPath)
            self.viewModel.toDeleteItem { (item) in
                self.viewModel.removeItemFromBasket(itemId: item.idItem)
                tableView.reloadData()
                self.viewModel.updateBasketInFirestore { (error) in
                    if error != nil {
                        print("Error updating the basket: ", error!.localizedDescription)
                    }
                    self.getBasketItems()
                }
            }
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        viewModel.selectRow(for: indexPath)
        let itemViewModel = viewModel.viewModelForSelected()
        let controller = ItemViewController()
        controller.viewModel = itemViewModel
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

// MARK: CardInfoViewControllerDelegate
extension BasketViewController: CardInfoViewControllerDelegate {
    func didClickDone(_ token: STPToken) {
        finisPayment(token: token)
    }

    func didClickCancel() {
        showNotitfication(text: "Payment Canceles", isError: true)
    }
}

// MARK: - Setup UI
extension BasketViewController {
    private func setupUI() {
        let topStackView = UIStackView(arrangedSubviews: [
            titleTotalItemsLabel, totalItemsLabel
        ])
        topStackView.spacing = 20

        let priceStackView = UIStackView(arrangedSubviews: [
               titleBasketTotalBasketLabel, basketTotalPriceLabel
        ])
        topStackView.spacing = 20

        let verticalStackView = UIStackView(arrangedSubviews: [
            topStackView, tableView, priceStackView, checkOutButtonOutlet
        ])
        view.addSubview(verticalStackView)
        verticalStackView.spacing = 15
        verticalStackView.axis = .vertical

        verticalStackView.snp.makeConstraints { (make) in
            make.top.equalTo(view.safeAreaLayoutGuide).offset(20)
            make.leading.equalTo(view).offset(20)
            make.trailing.equalTo(view).offset(-20)
            make.bottom.equalTo(view.safeAreaLayoutGuide).offset(-20)
        }
    }

    private func setupTableView() {
        tableView = UITableView(frame: .zero, style: .plain)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = .white
        tableView.sectionFooterHeight = UITableView.automaticDimension

        tableView.register(BasketTableViewCell.self, forCellReuseIdentifier: BasketTableViewCell.reuseId)
    }
}
