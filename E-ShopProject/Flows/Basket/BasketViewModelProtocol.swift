import Foundation
import Stripe

protocol BasketViewModelProtocol {
    func loadBasketFromFirestore(completion: @escaping() -> Void)
    func getBasketItems(comletion: @escaping(Error?) -> Void)
    func numberOfItems() -> Int
    func removeItemFromBasket(itemId: String)
    func returnBasketTotalPrice() -> String
    func addItemsToPurchaseHistory(itemIds: [String])
    func finisPayment(token: STPToken, completion: @escaping(Error?) -> Void)
    func emptyTheBasket(completion: @escaping(Error?) -> Void)
    func cellViewModel(indexPath: IndexPath) -> BasketCellViewModelProtocol?
    func viewModelForSelected() -> ItemViewModelProtocol?
    func selectRow(for indexPath: IndexPath)

    func toDeleteItem(completion: @escaping(Item) -> Void)
    func updateBasketInFirestore(completion: @escaping(Error?) -> Void)
}
