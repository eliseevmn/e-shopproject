import UIKit
import Stripe

protocol CardInfoViewControllerDelegate: class {
    func didClickDone(_ token: STPToken)
    func didClickCancel()
}

class CardInfoViewController: UIViewController {

    // MARK: - Outlets
    private let cancelButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Cancel", for: .normal)
        button.addTarget(self, action: #selector(handleCancelButtonTapped), for: .touchUpInside)
        return button
    }()
    private let doneButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Done", for: .normal)
        button.isEnabled = false
        button.addTarget(self, action: #selector(handleDoneButtonTaped), for: .touchUpInside)
        return button
    }()
    let paymentCardTextField = STPPaymentCardTextField()

    // MARK: - Variables
    weak var delegate: CardInfoViewControllerDelegate?

    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupUI()

        paymentCardTextField.delegate = self
    }

    // MARK: - Actions
    @objc private func handleCancelButtonTapped() {
        delegate?.didClickCancel()
        dismissView()
    }

    @objc private func handleDoneButtonTaped() {
        processCard()
    }

    // MARK: - Helpers
    private func dismissView() {
        self.dismiss(animated: true, completion: nil)
    }

    private func processCard() {
        let cardParams = STPCardParams()
        cardParams.number = paymentCardTextField.cardNumber
        cardParams.expMonth = paymentCardTextField.expirationMonth
        cardParams.expYear = paymentCardTextField.expirationYear
        cardParams.cvc = paymentCardTextField.cvc

        STPAPIClient.shared().createToken(withCard: cardParams) { (token, error) in
            guard let token = token else {
                return
            }

            if error == nil {
                self.delegate?.didClickDone(token)
                self.dismissView()
            } else {
                print(error?.localizedDescription ?? "Error proccesing card token")
            }
        }
    }
}

// MARK: - STPPaymentCardTextFieldDelegate
extension CardInfoViewController: STPPaymentCardTextFieldDelegate {
    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        doneButton.isEnabled = textField.isValid
    }
}

// MARK: - Setup UI
extension CardInfoViewController {
    private func setupUI() {
        view.addSubview(cancelButton)
        view.addSubview(doneButton)
        view.addSubview(paymentCardTextField)

        cancelButton.snp.makeConstraints { (make) in
            make.top.equalTo(view).offset(35)
            make.leading.equalTo(view).offset(20)
            make.width.equalTo(80)
            make.height.equalTo(40)
        }
        doneButton.snp.makeConstraints { (make) in
            make.top.equalTo(view).offset(35)
            make.trailing.equalTo(view).offset(-20)
            make.width.equalTo(80)
            make.height.equalTo(40)
        }
        paymentCardTextField.snp.makeConstraints { (make) in
            make.top.equalTo(view).offset(100)
            make.leading.equalTo(view).offset(30)
            make.trailing.equalTo(view).offset(-30)
        }
    }
}
