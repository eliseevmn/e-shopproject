import Foundation

class FinishRegistrationViewModel: FinishRegistrationViewModelProtocol {

    // MARK: - Finish registration function
    func finishOnBoarding(name: String,
                          surname: String,
                          address: String,
                          completion: @escaping(Error?) -> Void) {
        let withValues = [
            kFIRSTNAME: name,
            kLASTNAME: surname,
            kONBOARD: true,
            kFULLADRESS: address,
            kFULLNAME: (name + " " + surname)
            ] as [String: Any]

        FirestoreServiceUser.updateCurrentUserInFirestore(withValues: withValues) { (error) in
            if error == nil {
                completion(nil)
            } else {
                completion(error)
            }
        }
    }
}
