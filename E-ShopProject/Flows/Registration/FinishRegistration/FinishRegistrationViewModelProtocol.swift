import Foundation

protocol FinishRegistrationViewModelProtocol {
    func finishOnBoarding(name: String, surname: String, address: String, completion: @escaping(Error?) -> Void)
}
