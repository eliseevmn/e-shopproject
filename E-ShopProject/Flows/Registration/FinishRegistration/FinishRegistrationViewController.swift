import UIKit
import JGProgressHUD
import NVActivityIndicatorView

class FinishRegistrationViewController: UIViewController {

    // MARK: - Outlets
    private let titleLabel = CustomAuthLabel(title: "On Boarding", fontSize: 26, fontWeight: .bold, color: .black)
    private let nameTextField = CustomTextField(cornerRadius: 5, height: 50, fontSize: 16, labelText: "Введите свое имя")
    private let surnameTextField = CustomTextField(cornerRadius: 5, height: 50, fontSize: 16, labelText: "Введите свою фамилию")
    private let addressTextField = CustomTextField(cornerRadius: 5, height: 50, fontSize: 16, labelText: "Введите свой адресс")
    private let doneButton = UIButton(title: "Done", isShadow: true)
    private let returnButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "closeIcon"), for: .normal)
        return button
    }()

    // MARK: - Variables
    let hud = JGProgressHUD(style: .dark)
    private var viewModel: FinishRegistrationViewModelProtocol = FinishRegistrationViewModel()

    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        setupUI()

        nameTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        surnameTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        addressTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        doneButton.addTarget(self, action: #selector(handleDoneButtonTapped), for: .touchUpInside)
        returnButton.addTarget(self, action: #selector(handleReturnButtonTapped), for: .touchUpInside)

        let hideKeyboardGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(hideKeyboardGesture)
    }

    // MARK: - Actions
    @objc private func handleReturnButtonTapped() {
        self.dismiss(animated: true, completion: nil)
    }

    @objc private func handleDoneButtonTapped() {
        finishOnBoarding()
    }

    @objc private func textFieldDidChange() {
        updateDoneButtonStatus()
    }

    @objc private func hideKeyboard() {
        view.endEditing(true)
    }

    // MARK: - Update User
    private func finishOnBoarding() {
        guard let name = nameTextField.text,
            let surname = surnameTextField.text,
            let address = addressTextField.text else { return }

        viewModel.finishOnBoarding(name: name, surname: surname, address: address) { (error) in
            if error == nil {
                self.hudSuccess(text: "Updated!")
                self.dismiss(animated: true, completion: nil)
            } else {
                self.hudError(text: error?.localizedDescription ?? "Error updating user")
            }
        }
    }

    // MARK: - Helpers functions
    private func updateDoneButtonStatus() {
        if nameTextField.text != "" && surnameTextField.text != "" && addressTextField.text != "" {
            doneButton.backgroundColor = #colorLiteral(red: 0.9098039216, green: 0.2705882353, blue: 0.3529411765, alpha: 1)
            doneButton.isEnabled = true
        } else {
            doneButton.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            doneButton.isEnabled = false
        }
    }

    private func hudSuccess(text: String) {
        self.hud.textLabel.text = text
        self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
        self.hud.show(in: self.view)
        self.hud.dismiss(afterDelay: 2.0)
    }

    private func hudError(text: String) {
        self.hud.textLabel.text = text
        self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
        self.hud.show(in: self.view)
        self.hud.dismiss(afterDelay: 2.0)
    }
}

// MARK: - Setup UI
extension FinishRegistrationViewController {
    private func setupUI() {
        view.addSubview(returnButton)

        returnButton.snp.makeConstraints { (make) in
            make.top.equalTo(view.safeAreaLayoutGuide).offset(20)
            make.trailing.equalTo(view).offset(-20)
            make.width.height.equalTo(40)
        }

        let registrationStackView = UIStackView(arrangedSubviews: [
            titleLabel, nameTextField, surnameTextField, addressTextField
        ])
        registrationStackView.axis = .vertical
        registrationStackView.spacing = 15

        let overlayStackView = UIStackView(arrangedSubviews: [
            registrationStackView,
            doneButton
        ])
        view.addSubview(overlayStackView)
        overlayStackView.axis = .vertical
        overlayStackView.spacing = 40

        overlayStackView.snp.makeConstraints { (make) in
            make.top.equalTo(view.safeAreaLayoutGuide).offset(80)
            make.leading.equalTo(view).offset(48)
            make.trailing.equalTo(view).offset(-48)
        }
    }
}
