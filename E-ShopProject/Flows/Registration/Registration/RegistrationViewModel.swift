import Foundation

class RegistrationViewModel: RegistrationViewModelProtocol {

    // MARK: - Register User
    func resendButtonTapped(email: String, completion: @escaping(Error?) -> Void) {
        FirestoreServiceLogin.resendVerificationEmail(email: email) { (error) in
            completion(error)
        }
    }

   func loginUser(email: String, password: String, completion: @escaping(_ error: Error?, _ isEmailVerified: Bool) -> Void) {
        FirestoreServiceLogin.loginUserWith(email: email, password: password) { (error, isEmailVerified) in
            if error == nil {
                completion(nil, isEmailVerified)
            } else {
                completion(error, isEmailVerified)
            }
        }
    }

    func registerUser(email: String, password: String, completion: @escaping(Error?) -> Void) {
        FirestoreServiceLogin.registerUserWith(email: email, password: password) { (error) in
            if let error = error {
                completion(error)
                return
            } else {
                completion(nil)
            }
        }
    }

    // MARK: - Helpers functions
    func resetThePassword(email: String, completion: @escaping(Error?) -> Void) {
        FirestoreServiceLogin.resetPasswordFor(email: email) { (error) in
            if error == nil {
                completion(error)
            } else {
                completion(nil)
            }
        }
    }
}
