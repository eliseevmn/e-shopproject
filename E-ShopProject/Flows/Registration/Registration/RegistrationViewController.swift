import UIKit
import JGProgressHUD
import NVActivityIndicatorView

class RegistrationViewController: UIViewController {

    // MARK: - Outlets
    private let titleLabel = CustomAuthLabel(title: "Зарегистрироваться", fontSize: 26, fontWeight: .bold, color: .black)
    private let emailTextField = CustomTextField(cornerRadius: 5, height: 50, fontSize: 16, labelText: "Введите свой email")
    private let passwordTextField = CustomTextField(cornerRadius: 5, height: 50, fontSize: 16, labelText: "Введите свой пароль")
    private let registrationButton = UIButton(title: "Register", isShadow: true)
    private let loginButton = UIButton(title: "Login", isShadow: true)
    private let forgetPasswordButton = UIButton(title: "Forget password?", isShadow: true, isBorder: true)
    private let resendButton = UIButton(title: "Resend password", isShadow: false, isBorder: true)
    private let returnButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "closeIcon"), for: .normal)
        return button
    }()

    // MARK: - Variables
    let hud = JGProgressHUD(style: .dark)
    var activityIndicator: NVActivityIndicatorView?
    private var viewModel: RegistrationViewModelProtocol = RegistrationViewModel()

    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        setupUI()

        returnButton.addTarget(self, action: #selector(handleReturnButtonTapped), for: .touchUpInside)
        loginButton.addTarget(self, action: #selector(handleLoginButtonTapped), for: .touchUpInside)
        registrationButton.addTarget(self, action: #selector(handleRegistrationButtonTapped), for: .touchUpInside)
        forgetPasswordButton.addTarget(self, action: #selector(handleForgotPasswordButtonTapped), for: .touchUpInside)
        resendButton.addTarget(self, action: #selector(handleResendButtonTapped), for: .touchUpInside)

        let hideKeyboardGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(hideKeyboardGesture)
    }

    // MARK: - Actions
    @objc private func handleReturnButtonTapped() {
        self.dismiss(animated: true, completion: nil)
    }

    @objc private func handleLoginButtonTapped() {
        if textFieldsHaveText() {
            loginUser()
        } else {
            hudError(text: "All fields are required!")
        }
    }

    @objc private func handleRegistrationButtonTapped() {
        if textFieldsHaveText() {
            registerUser()
        } else {
            hudError(text: "All fields are required")
        }
    }

    @objc private func handleForgotPasswordButtonTapped() {
        if emailTextField.text != "" {
            resetThePassword()
        } else {
            hudError(text: "Please insert email!")
        }
    }

    @objc private func handleResendButtonTapped() {
        guard let email = emailTextField.text else { return }
        viewModel.resendButtonTapped(email: email) { error in
             self.hudError(text: error?.localizedDescription ?? "Error resending email!")
        }
    }

    @objc private func hideKeyboard() {
        view.endEditing(true)
    }

    // MARK: - Login User
    private func loginUser() {
        guard let email = emailTextField.text,
            let password = passwordTextField.text else { return }
        showLoadingIndicator()
        viewModel.loginUser(email: email, password: password) { (error, isEmailVerified)  in
            if error == nil {
                if isEmailVerified {
                    self.dismiss(animated: true, completion: nil)
                } else {
                    self.hudError(text: "Please Verify your email!")
                    self.resendButton.isHidden = false
                }
            } else {
                self.hudError(text: error?.localizedDescription ?? "Error loging in the user")
            }
            self.hideLoadingIndicator()
        }
    }

    // MARK: - Register User
    private func registerUser() {
        guard let email = emailTextField.text,
            let password = passwordTextField.text else { return }
        showLoadingIndicator()
        viewModel.registerUser(email: email, password: password) { (error) in
            if let error = error {
                self.hudError(text: error.localizedDescription)
                self.hideLoadingIndicator()
            } else {
                self.hudSuccess(text: "Verification Email sent!")
            }
            self.hideLoadingIndicator()
        }
    }

    // MARK: - Helpers functions
    private func resetThePassword() {
        guard let email = emailTextField.text else { return }
        viewModel.resetThePassword(email: email) { (error) in
            if error == nil {
                self.hudSuccess(text: "Reset password email sent!")
            } else {
                self.hudError(text: error?.localizedDescription ?? "Проблема с отправкой сообщений")
            }
        }
    }

    private func hudSuccess(text: String) {
        self.hud.textLabel.text = text
        self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
        self.hud.show(in: self.view)
        self.hud.dismiss(afterDelay: 2.0)
    }

    private func hudError(text: String) {
        self.hud.textLabel.text = text
        self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
        self.hud.show(in: self.view)
        self.hud.dismiss(afterDelay: 2.0)
    }

    private func textFieldsHaveText() -> Bool {
        return emailTextField.text != "" && passwordTextField.text != ""
    }

    private func clearTextfields() {
        emailTextField.text = ""
        passwordTextField.text = ""
    }

    // MARK: - Activity Indicator
    private func showLoadingIndicator() {
        if let activityIndicator = activityIndicator {
            self.view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
        }
    }

    private func hideLoadingIndicator() {
        if let activityIndicator = activityIndicator {
            activityIndicator.removeFromSuperview()
            activityIndicator.stopAnimating()
        }
    }
}

// MARK: - Setup UI
extension RegistrationViewController {
    private func setupUI() {
        resendButton.isHidden = true

        view.addSubview(returnButton)
        returnButton.snp.makeConstraints { (make) in
            make.top.equalTo(view.safeAreaLayoutGuide).offset(20)
            make.trailing.equalTo(view).offset(-20)
            make.width.height.equalTo(40)
        }

        let registrationStackView = UIStackView(arrangedSubviews: [
            titleLabel, emailTextField, passwordTextField
        ])
        registrationStackView.axis = .vertical
        registrationStackView.spacing = 15

        let overlayStackView = UIStackView(arrangedSubviews: [
            registrationStackView,
            loginButton,
            registrationButton,
            resendButton
        ])
        view.addSubview(overlayStackView)
        overlayStackView.axis = .vertical
        overlayStackView.spacing = 40

        overlayStackView.snp.makeConstraints { (make) in
            make.top.equalTo(view.safeAreaLayoutGuide).offset(80)
            make.leading.equalTo(view).offset(48)
            make.trailing.equalTo(view).offset(-48)
        }

        view.addSubview(forgetPasswordButton)
        forgetPasswordButton.snp.makeConstraints { (make) in
            make.leading.equalTo(view).offset(48)
            make.trailing.equalTo(view).offset(-48)
            make.bottom.equalTo(view.safeAreaLayoutGuide).offset(-20)
        }
    }
}
