import Foundation

protocol RegistrationViewModelProtocol {
    func resendButtonTapped(email: String, completion: @escaping(Error?) -> Void)
    func registerUser(email: String, password: String, completion: @escaping(Error?) -> Void)
    func resetThePassword(email: String, completion: @escaping(Error?) -> Void)
    func loginUser(email: String, password: String, completion: @escaping(_ error: Error?, _ isEmailVerified: Bool) -> Void)
}
