import Foundation

class EditViewModel: EditViewModelProtocol {

    // MARK: - Update function in firebase
    func updateCurrentUserInFirestore(
        name: String?,
        surname: String?,
        address: String?,
        completion: @escaping(Error?) -> Void) {

        let withValues = [kFIRSTNAME: name!, kLASTNAME: surname!, kFULLADRESS: address!, kFULLNAME: (name! + " " + surname!)] as [String: Any]
        FirestoreServiceUser.updateCurrentUserInFirestore(withValues: withValues) { (error) in
            if error == nil {
                completion(nil)
            } else {
                completion(error)
            }
        }
    }

    func finishOnBoarding(
        name: String?,
        surname: String?,
        address: String?,
        completion: @escaping(Error?) -> Void) {

        let withValues = [
            kFIRSTNAME: name!,
            kLASTNAME: surname!,
            kONBOARD: true,
            kFULLADRESS: address!,
            kFULLNAME: (name! + " " + surname!)
            ] as [String: Any]

        FirestoreServiceUser.updateCurrentUserInFirestore(withValues: withValues) { (error) in
            if error == nil {
                completion(nil)
            } else {
                completion(error)
            }
        }
    }

    // MARK: - Logout user from app
    func logOutUser(completion: @escaping(Error?) -> Void) {
        FirestoreServiceLogin.logOutCurrentUser { (error) in
            if error == nil {
                completion(nil)
            } else {
                completion(error)
            }
        }
    }
}
