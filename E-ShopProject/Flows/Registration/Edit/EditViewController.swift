import UIKit
import JGProgressHUD
import NVActivityIndicatorView

class EditViewController: UIViewController {

    // MARK: - Outlets
    private let nameTextField = CustomEditTextField(cornerRadius: 5, height: 50, fontSize: 16, placeholderText: "Введите свое имя")
    private let surnameTextField = CustomEditTextField(cornerRadius: 5, height: 50, fontSize: 16, placeholderText: "Введите свою фамилию")
    private let addressTextField = CustomEditTextField(cornerRadius: 5, height: 50, fontSize: 16, placeholderText: "Введите свой адресс")
    private let logoutButton = UIButton(title: "Log Out", isShadow: true)

    // MARK: - Variables
    let hud = JGProgressHUD(style: .dark)
    private var viewModel: EditViewModelProtocol = EditViewModel()

    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        title = "Edit Profile"
        setupUI()
        createBarButtonItem()

        loadUserInfo()

        logoutButton.addTarget(self, action: #selector(handleLogoutButtonTapped), for: .touchUpInside)

        let hideKeyboardGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(hideKeyboardGesture)
    }

    // MARK: - Actions
    @objc private func handleLogoutButtonTapped() {
        logOutUser()
    }

    @objc private func hideKeyboard() {
        dismissKeyboard()
    }

    @objc private func handleSaveBarButtonTapped() {
        dismissKeyboard()

        if textFieldsHaveText() {
            viewModel.updateCurrentUserInFirestore(name: nameTextField.text,
                                                   surname: surnameTextField.text!,
                                                   address: addressTextField.text!) { (error) in
                if error == nil {
                    self.hudSuccess(text: "Updated!")
                } else {
                    self.hudError(text: error?.localizedDescription ?? "Error updating user")
                }
            }
        } else {
            hudError(text: "All fields are required!")
        }
    }

    // MARK: - Update User
    private func finishOnBoarding() {
        guard let name = nameTextField.text,
            let surname = surnameTextField.text,
            let address = addressTextField.text else { return }

        viewModel.finishOnBoarding(name: name, surname: surname, address: address) { (error) in
            if error == nil {
                self.hudSuccess(text: "Updated!")
                self.dismiss(animated: true, completion: nil)
            } else {
                self.hudError(text: error?.localizedDescription ?? "Error updating user")
            }
        }
    }

    // MARK: - UpdtaeUI
    private func loadUserInfo() {
        if MUser.currentUser() != nil {
            let currentUser = MUser.currentUser()!
            nameTextField.text = currentUser.firstName
            surnameTextField.text = currentUser.lastName
            addressTextField.text = currentUser.fullAddress
        }
    }

    // MARK: - Helpers functions
    private func logOutUser() {
        viewModel.logOutUser { (error) in
            if error == nil {
                self.navigationController?.popViewController(animated: true)
            } else {
                self.hudError(text: error?.localizedDescription ?? "Error logout!")
            }
        }
    }

    private func dismissKeyboard() {
        self.view.endEditing(false)
    }

    private func textFieldsHaveText() -> Bool {
        return (nameTextField.text != "" && surnameTextField.text != "" && addressTextField.text != "")
    }

    private func hudSuccess(text: String) {
        self.hud.textLabel.text = text
        self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
        self.hud.show(in: self.view)
        self.hud.dismiss(afterDelay: 2.0)
    }

    private func hudError(text: String) {
        self.hud.textLabel.text = text
        self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
        self.hud.show(in: self.view)
        self.hud.dismiss(afterDelay: 2.0)
    }
}

// MARK: - Setup UI
extension EditViewController {
    private func setupUI() {

        let registrationStackView = UIStackView(arrangedSubviews: [
            nameTextField, surnameTextField, addressTextField
        ])
        registrationStackView.axis = .vertical
        registrationStackView.spacing = 15

        let overlayStackView = UIStackView(arrangedSubviews: [
            registrationStackView,
            logoutButton
        ])
        view.addSubview(overlayStackView)
        overlayStackView.axis = .vertical
        overlayStackView.spacing = 40

        overlayStackView.snp.makeConstraints { (make) in
            make.top.equalTo(view.safeAreaLayoutGuide).offset(80)
            make.leading.equalTo(view).offset(48)
            make.trailing.equalTo(view).offset(-48)
        }
    }

    private func createBarButtonItem() {
        let saveBarButtonItems = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(handleSaveBarButtonTapped))
        navigationItem.rightBarButtonItem = saveBarButtonItems
    }
}
