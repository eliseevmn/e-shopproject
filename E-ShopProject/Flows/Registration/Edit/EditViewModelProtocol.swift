import Foundation

protocol EditViewModelProtocol {
    func updateCurrentUserInFirestore(name: String?, surname: String?, address: String?, completion: @escaping(Error?) -> Void)
    func finishOnBoarding(name: String?, surname: String?, address: String?, completion: @escaping(Error?) -> Void)
    func logOutUser(completion: @escaping(Error?) -> Void)
}
