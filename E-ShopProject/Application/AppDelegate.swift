import UIKit
import Firebase
import Stripe
import SnapKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        iniializeStripe()

        return true
    }

    // MARK: UISceneSession Lifecycle
    func application(_ application: UIApplication,
                     configurationForConnecting connectingSceneSession: UISceneSession,
                     options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {

    }

    // MARK: - PayPal Init
    func initializePayPal() {
        PayPalMobile.initializeWithClientIds(
            forEnvironments: [PayPalEnvironmentProduction: "ARIpHNJ0R7ZgtHCnnsuzTQpySjtbLvRK6ChIaMq6eOPHHvpWBa7SKE4U_fFNy7Kj_-M15zmLcXFf_dFf",
                              PayPalEnvironmentSandbox: "sb-wm9as2427728@personal.example.com"])
    }

    func iniializeStripe() {
        STPAPIClient.shared().publishableKey = Constants.publishableKey
//        STPPaymentConfiguration.shared().publishableKey = Constants.publishableKey
        StripeClient.shared.baseURLString = Constants.baseURLString

    }
}
