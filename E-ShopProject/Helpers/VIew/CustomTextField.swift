import UIKit

class CustomTextField: UITextField {

    // MARK: - Variables
    let cornerRadius: CGFloat
    let height: CGFloat
    let fontSize: CGFloat
    private let defaultUnderlineColor = UIColor.black
    private var labelYAnchorConstraint: NSLayoutConstraint!
    private var labelLeadingAnchor: NSLayoutConstraint!

    // MARK: - Outlets
    let placeholderText: UILabel = {
        let label = UILabel()
        label.textColor = .gray
        label.alpha = 0.6
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    private let bottomLine = UIView()

    // MARK: - Init
    init(cornerRadius: CGFloat, height: CGFloat, fontSize: CGFloat, labelText: String) {
        self.cornerRadius = cornerRadius
        self.height = height
        self.fontSize = fontSize
        self.placeholderText.text = labelText
        super.init(frame: .zero)
        textColor = .gray
        layer.cornerRadius = cornerRadius
        font = UIFont.boldSystemFont(ofSize: fontSize)
        configureViews()
        delegate = self
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Setup UI
    override var intrinsicContentSize: CGSize {
        return .init(width: 0, height: height)
    }

    public func setUnderlineColor(color: UIColor = .red) {
        bottomLine.backgroundColor = color
    }

    public func setDefaultUnderlineColor() {
        bottomLine.backgroundColor = defaultUnderlineColor
    }

    func configureViews() {
        borderStyle = .none
        bottomLine.backgroundColor = defaultUnderlineColor

        self.addSubview(bottomLine)
        bottomLine.snp.makeConstraints { (make) in
            make.leading.equalTo(self)
            make.trailing.equalTo(self)
            make.bottom.equalTo(self)
            make.height.equalTo(1)
            make.width.equalTo(frame.width)
        }

        self.addSubview(placeholderText)
        labelYAnchorConstraint = placeholderText.centerYAnchor.constraint(equalTo: centerYAnchor)
        labelLeadingAnchor = placeholderText.leadingAnchor.constraint(equalTo: leadingAnchor)
        NSLayoutConstraint.activate([
            labelLeadingAnchor,
            labelYAnchorConstraint
        ])
    }
}

// MARK: - UITextFieldDelegate
extension CustomTextField: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        labelYAnchorConstraint.constant = -30
        labelLeadingAnchor.constant = -15
        placeholderText.textColor = .red
        placeholderText.alpha = 0.6
        performAnimation(transform: CGAffineTransform(scaleX: 0.8, y: 0.8))
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if let text = textField.text, text.isEmpty {
            labelYAnchorConstraint.constant = 0
            labelLeadingAnchor.constant = 0
            placeholderText.textColor = .black
            placeholderText.alpha = 0.5
            performAnimation(transform: CGAffineTransform(scaleX: 1, y: 1))
        }
    }

    fileprivate func performAnimation(transform: CGAffineTransform) {
        UIView.animate(withDuration: 0.5,
                       delay: 0,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 1,
                       options: .curveEaseOut,
                       animations: {
            self.placeholderText.transform = transform
            self.layoutIfNeeded()
        }, completion: nil)
    }
}
