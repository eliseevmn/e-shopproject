import UIKit

class CustomEditTextField: UITextField {

    // MARK: - Variables
    let cornerRadius: CGFloat
    let height: CGFloat
    let fontSize: CGFloat
    private let defaultUnderlineColor = UIColor.black

    // MARK: - Outlets
    private let bottomLine = UIView()
    private let placeholderText: String

    // MARK: - Init
    init(cornerRadius: CGFloat, height: CGFloat, fontSize: CGFloat, placeholderText: String) {
        self.cornerRadius = cornerRadius
        self.height = height
        self.fontSize = fontSize
        self.placeholderText = placeholderText
        super.init(frame: .zero)
        textColor = .gray
        placeholder = placeholderText
        layer.cornerRadius = cornerRadius
        font = UIFont.boldSystemFont(ofSize: fontSize)
        configureViews()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Setup UI
    override var intrinsicContentSize: CGSize {
        return .init(width: 0, height: height)
    }

    public func setUnderlineColor(color: UIColor = .red) {
        bottomLine.backgroundColor = color
    }

    public func setDefaultUnderlineColor() {
        bottomLine.backgroundColor = defaultUnderlineColor
    }

    func configureViews() {
        borderStyle = .none
        bottomLine.backgroundColor = defaultUnderlineColor

        self.addSubview(bottomLine)
        bottomLine.snp.makeConstraints { (make) in
            make.leading.equalTo(self)
            make.trailing.equalTo(self)
            make.bottom.equalTo(self)
            make.height.equalTo(1)
            make.width.equalTo(frame.width)
        }
    }
}
