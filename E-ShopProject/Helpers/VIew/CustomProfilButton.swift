import UIKit

class CustomProfileButton: UIButton {

    // MARK: - Outlets
    let borderView: UIView = {
        let view = UIView()
        return view
    }()

    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: .zero)
        contentHorizontalAlignment = .left
        contentEdgeInsets = UIEdgeInsets(top: 0, left: -40, bottom: 0, right: 0)

        setImage(#imageLiteral(resourceName: "more"), for: .normal)

        setTitleColor(#colorLiteral(red: 0.03921568627, green: 0.5176470588, blue: 1, alpha: 1), for: .normal)
        setTitleColor(#colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1), for: .disabled)
        setupBottomBorder()

        borderView.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Image sizes
    override func imageRect(forContentRect contentRect: CGRect) -> CGRect {
        var imageFrame = super.imageRect(forContentRect: contentRect)
        imageFrame.origin.x = self.frame.width - 50
        return imageFrame
    }

    // MARK: - Add bottom border
    private func setupBottomBorder() {
        addSubview(borderView)
        borderView.snp.makeConstraints { (make) in
            make.leading.equalTo(self)
            make.trailing.equalTo(self).offset(-60)
            make.bottom.equalTo(self)
            make.height.equalTo(1)
        }
    }
}
