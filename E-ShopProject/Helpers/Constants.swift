import Foundation

enum Constants {
    static let publishableKey = ""
    static let baseURLString = "http://localhost:3000"
    static let defaultCurrency = "usd"
    static let defaultDescription = "Purchase from market"
}

//IDS and Keys
public let kFILEPREFERENCE = "gs://e-shop-1b0d3.appspot.com/"
public let kAlgoliaAppId = ""
public let kAlgoliaSearchKey = ""
public let kAlgoliaAdminKey = ""

//Firebase Headers
public let kUserPath = "User"
public let kCategoryPath = "Category"
public let kItemsPath = "Items"
public let kBasketPath = "Basket"

//Category
public let kNAME = "name"
public let kIMAGENAME = "imageName"
public let kOBJECTID = "objectId"

//Item
public let kCATEGORYID = "categoryId"
public let kDESCRIPTION = "description"
public let kPRICE = "price"
public let kIMAGELINKS = "imageLinks"

//Basket
public let kOWNERID = "ownerId"
public let kITEMSIDS = "itemIds"

//Muser
public let kEMAIL = "email"
public let kFIRSTNAME = "firstName"
public let kLASTNAME = "lastName"
public let kFULLNAME = "fullName"
public let kCURRENTUSER = "currentUser"
public let kFULLADRESS = "fullAdress"
public let kONBOARD = "onBoard"
public let kPURCHASEDITEMSIDS = "purchasedItemIds"
