import Foundation

class Validators {

    static func isFillled(email: String?, password: String?) -> Bool {
        guard !(email ?? "").isEmpty,
            !(password ?? "").isEmpty else {
                return false
        }
        return true
    }

    static func isFillledEmail(email: String?) -> Bool {
        guard !(email ?? "").isEmpty else {
                return false
        }
        return true
    }

    static func isSimpleEmail(_ email: String) -> Bool {
        let emailRegEx = "^.+@.+\\..{2,}$"
        return check(text: email, regEx: emailRegEx)
    }

    public static func check(text: String, regEx: String) -> Bool {
        let predicate = NSPredicate(format: "SELF MATCHES %@", regEx)
        return predicate.evaluate(with: text)
    }
}
