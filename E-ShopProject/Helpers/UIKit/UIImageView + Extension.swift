import UIKit

extension UIImageView {

    convenience init(image: UIImage?, contentMode: UIView.ContentMode, tintColor: UIColor) {
        self.init()
        self.image = image
        self.contentMode = contentMode
        self.tintColor = tintColor

        layer.cornerRadius = 6
        layer.masksToBounds = true
        heightAnchor.constraint(equalToConstant: 100).isActive = true
    }
}
