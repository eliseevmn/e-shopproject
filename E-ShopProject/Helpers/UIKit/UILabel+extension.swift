import UIKit

extension UILabel {

    convenience init(title: String) {
        self.init()
        self.text = title
    }
}

extension UILabel {

    convenience init(title: String, width: CGFloat) {
        self.init()
        self.text = title
        self.widthAnchor.constraint(equalToConstant: width).isActive = true
    }
}

extension UILabel {

    convenience init(title: String, alignment: NSTextAlignment) {
        self.init()
        self.text = title
        self.textAlignment = alignment
    }
}
