import Foundation
import UIKit

extension UIButton {

    convenience init(title: String,
                     isShadow: Bool = false,
                     isBorder: Bool = false) {
        self.init(type: .system)

        self.setTitle(title, for: .normal)
        self.setTitleColor(.white, for: .normal)
        self.backgroundColor = #colorLiteral(red: 0.9098039216, green: 0.2705882353, blue: 0.3529411765, alpha: 1)
        self.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        self.heightAnchor.constraint(equalToConstant: 50).isActive = true

        if isBorder {
            self.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.layer.borderWidth = 1
            self.layer.borderColor = #colorLiteral(red: 0.9098039216, green: 0.2705882353, blue: 0.3529411765, alpha: 1)
            self.setTitleColor(#colorLiteral(red: 0.9098039216, green: 0.2705882353, blue: 0.3529411765, alpha: 1), for: .normal)
        }

        if isShadow {
            self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowRadius = 4
            self.layer.shadowOpacity = 0.2
            self.layer.shadowOffset = CGSize(width: 0, height: 4)
        }
    }
}
