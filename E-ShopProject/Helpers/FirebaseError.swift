import Foundation

enum FirebaseError {
    case errorUpdatingBasket
}

extension FirebaseError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .errorUpdatingBasket:
            return NSLocalizedString("Error updating basket", comment: "")
        }
    }
}
