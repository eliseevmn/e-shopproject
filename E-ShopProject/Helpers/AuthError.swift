import Foundation

enum AuthError {
    case notFill
    case invalidEmail
    case unknownError
    case serverError
    case confirmPassword
}

extension AuthError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .notFill:
            return NSLocalizedString("Заполните все поля", comment: "")
        case .invalidEmail:
            return NSLocalizedString("Неправильный Email", comment: "")
        case .unknownError:
            return NSLocalizedString("Неизвестная ошибка", comment: "")
        case .serverError:
            return NSLocalizedString("Ошибка сервера", comment: "")
        case .confirmPassword:
            return NSLocalizedString("Пароли не совпадают", comment: "")
        }
    }
}
