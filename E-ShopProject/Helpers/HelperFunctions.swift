import UIKit

class HelperFunctions {

    static func convertToCurrency(_ number: Double) -> String {

        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.locale = Locale.current

        guard let priceString = currencyFormatter.string(from: NSNumber(value: number)) else {
            return "Error"
        }
        return "$ " + priceString
    }
}
