import Foundation
import FirebaseFirestore

enum FCollectionReference: String {
    case user = "User"
    case category = "Category"
    case items = "Items"
    case basket = "Basket"
}

func firebaseReference(_ collectionReference: FCollectionReference) -> CollectionReference {
    return Firestore.firestore().collection(collectionReference.rawValue)
}
